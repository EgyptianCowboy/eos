/*
    Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
    Copyright (C) 2012 - 2018 Xilinx, Inc. All Rights Reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software. If you wish to use our Amazon
    FreeRTOS name, please do so in a fair use way that does not cause confusion.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    http://www.FreeRTOS.org
    http://aws.amazon.com/freertos


    1 tab == 4 spaces!
*/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
//#include "timers.h"
/* Xilinx includes. */
#include "xil_printf.h"
#include "xparameters.h"
#include "sevenSegDriver.h"
#include "xgpiops.h"
#include "xuartps.h"

#define UART_DEVICE_ID              XPAR_XUARTPS_1_DEVICE_ID
#define TEST_BUFFER_SIZE 1
//static u8 SendBuffer[32];	/* Buffer for Transmitting Data */
static u8 RecvBuffer[TEST_BUFFER_SIZE];	/* Buffer for Receiving Data */

#define DELAY_1_SECOND		1000UL

#define SSEGBASE XPAR_SEVENSEGDRIVER_0_S00_AXI_BASEADDR
#define SSEG_0 SEVENSEGDRIVER_S00_AXI_SLV_REG0_OFFSET

#define GPIO_DEVICE_ID		XPAR_XGPIOPS_0_DEVICE_ID
#define SWITCH_PS 0
/*-----------------------------------------------------------*/

/* The Tx and Rx tasks as described at the top of this file. */
static XGpioPs Gpio;
XUartPs_Config *uartConfig;
XUartPs Uart_PS;

static void sSegWrite( void *pvParameters );
static void uartRead( void *pvParameters );
static void switchRead( void *pvParameters );
//static void vTimerCallback( TimerHandle_t pxTimer );
/*-----------------------------------------------------------*/

/* The queue used by the Tx and Rx tasks, as described at the top of this
file. */
static TaskHandle_t xsSegWrite;
static TaskHandle_t xuartRead;
static TaskHandle_t xswitchRead;
static QueueHandle_t xQueue = NULL;
//static TimerHandle_t xTimer = NULL;
u8 queueNumber=0;
long RxtaskCntr = 0;

int main( void )
{
	//const TickType_t x10seconds = pdMS_TO_TICKS( DELAY_10_SECONDS );
	uartConfig = XUartPs_LookupConfig(XPAR_XUARTPS_1_DEVICE_ID);
	XUartPs_CfgInitialize(&Uart_PS, uartConfig, uartConfig->BaseAddress);
	XUartPs_SetOperMode(&Uart_PS, XUARTPS_OPER_MODE_AUTO_ECHO);

	XGpioPs_Config *ConfigPtr;
	ConfigPtr = XGpioPs_LookupConfig(GPIO_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);
	XGpioPs_SetDirectionPin(&Gpio, SWITCH_PS, 0x0);

	xil_printf( "Running program\r\n" );
	xil_printf( "Hello from Freertos example main\r\n" );

	/* Create the two tasks.  The Tx task is given a lower priority than the
	Rx task, so the Rx task will leave the Blocked state and pre-empt the Tx
	task as soon as the Tx task places an item in the queue. */
	xTaskCreate( 	sSegWrite, 					/* The function that implements the task. */
					( const char * ) "sSeg", 		/* Text name for the task, provided to assist debugging only. */
					configMINIMAL_STACK_SIZE, 	/* The stack allocated to the task. */
					NULL, 						/* The task parameter is not used, so set to NULL. */
					tskIDLE_PRIORITY,			/* The task runs at the idle priority. */
					&xsSegWrite );

	xTaskCreate( uartRead,
				 ( const char * ) "uart",
				 configMINIMAL_STACK_SIZE,
				 NULL,
				 tskIDLE_PRIORITY + 1,
				 &xuartRead );

	xTaskCreate( switchRead,
			( const char * ) "uart",
			configMINIMAL_STACK_SIZE,
			NULL,
			tskIDLE_PRIORITY + 2,
			&xswitchRead );

	/* Create the queue used by the tasks.  The Rx task has a higher priority
	than the Tx task, so will preempt the Tx task and remove values from the
	queue as soon as the Tx task writes to the queue - therefore the queue can
	never have more than one item in it. */
	xQueue = xQueueCreate( 	5,						/* There is only one space in the queue. */
							sizeof( &queueNumber ) );	/* Each space in the queue is large enough to hold a uint32_t. */

	/* Check the queue was created. */
	configASSERT( xQueue );

	/* Start the tasks and timer running. */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	for( ;; );
}


/*-----------------------------------------------------------*/
static void sSegWrite( void *pvParameters )
{
	u8 recvData;
	for( ;; )
	{
		xQueueReceive( 	xQueue,				/* The queue being read. */
						&recvData,	/* Data is read into this address. */
						portMAX_DELAY );	/* Wait without a timeout for data. */
		xil_printf("Recv data is %d\r\n", recvData);
		SEVENSEGDRIVER_mWriteReg(SSEGBASE,SSEG_0,recvData<<28);
	}
}

static void uartRead( void *pvParameters )
{
	for( ;; )
	{
		XUartPs_Recv(&Uart_PS, &RecvBuffer[0],1);

		if(RecvBuffer[0] >= 48 && RecvBuffer[0] <= 57) {
			char* sendString = "A number\r\n";
			XUartPs_Send(&Uart_PS, (u8*)&sendString[0],sizeof(sendString));
			u8 writeNumber = RecvBuffer[0] - 48;

			xQueueSend( xQueue,			/* The queue being written to. */
						&writeNumber, /* The address of the data being sent. */
						0UL );			/* The block time. */
		} else {
			char* sendString = "Not a number\r\n";
			XUartPs_Send(&Uart_PS, (u8*)&sendString[0],sizeof(sendString));
		}

		/* Send the next value on the queue.  The queue should always be
		empty at this point so a block time of 0 is used. */
		//xQueueSend( xQueue,			/* The queue being written to. */
		//			HWstring, /* The address of the data being sent. */
		//			0UL );			/* The block time. */
	}
}

static void switchRead( void *pvParameters )
{
	u8 newSwData = 0;
	u8 oldSwData = 0;
	u8 zero = 0;
	for( ;; )
	{
		newSwData = XGpioPs_ReadPin(&Gpio, SWITCH_PS);

		if((newSwData != oldSwData) & (newSwData == 1)) {
			xil_printf("Read pin\r\n");
			xQueueSend( xQueue,			/* The queue being written to. */
						&zero, /* The address of the data being sent. */
						0UL );			/* The block time. */
		}
		oldSwData = newSwData;
	}
}
/*-----------------------------------------------------------*/
/*static void vTimerCallback( TimerHandle_t pxTimer )
{
	long lTimerId;
	configASSERT( pxTimer );

	lTimerId = ( long ) pvTimerGetTimerID( pxTimer );

	if (lTimerId != TIMER_ID) {
		xil_printf("FreeRTOS Hello World Example FAILED");
	}
	if (RxtaskCntr >= TIMER_CHECK_THRESHOLD) {
		xil_printf("FreeRTOS Hello World Example PASSED");
	} else {
		xil_printf("FreeRTOS Hello World Example FAILED");
	}

	vTaskDelete( xRxTask );
	vTaskDelete( xTxTask );
}
*/

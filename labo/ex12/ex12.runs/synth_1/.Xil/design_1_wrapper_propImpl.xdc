set_property SRC_FILE_INFO {cfile:/home/sil/Documents/school/EOS/labo/ex12/ex12.srcs/constrs_1/imports/EOS/sevenSeg.xdc rfile:../../../ex12.srcs/constrs_1/imports/EOS/sevenSeg.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M15 [get_ports {outSeg_0[0]}];  # "M15.PMOD1_D0_N"
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L15 [get_ports {outSeg_0[1]}];  # "L15.PMOD1_D0_P"
set_property src_info {type:XDC file:1 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M14 [get_ports {outSeg_0[2]}];  # "M14.PMOD1_D1_N"
set_property src_info {type:XDC file:1 line:4 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L14 [get_ports {outSeg_0[3]}];  # "L14.PMOD1_D1_P"
set_property src_info {type:XDC file:1 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L13 [get_ports {outSeg_0[4]}];  # "L13.PMOD1_D2_N"
set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN K13 [get_ports {outSeg_0[5]}];  # "K13.PMOD1_D2_P"
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN N14 [get_ports {outSeg_0[6]}];  # "N14.PMOD1_D3_N"
set_property src_info {type:XDC file:1 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN N13 [get_ports {outSeg_0[7]}];  # "N13.PMOD1_D3_P"

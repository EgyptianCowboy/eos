library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sr04_fsm is
  port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    trig      : in  std_logic;
    echo      : in  std_logic;
    triglatch : out std_logic;
    ready     : out std_logic;
    distout   : out std_logic_vector(23 downto 0));
end entity sr04_fsm;

architecture behavorial of sr04_fsm is
  type sr04_state is (trigwait, trighold, echowait, echocount);
  signal present_state, next_state : sr04_state;

  signal trigcounter : std_logic_vector(11 downto 0) := (others => '0');
  signal distcounter : std_logic_vector(23 downto 0) := (others => '0');

  signal echo_synced, echo_unsynced, echo_last : std_logic;
  signal trig_synced, trig_unsynced, trig_last : std_logic;
begin  -- architecture behavorial

  -- purpose: Sequential part of the state machine
  -- type   : sequential
  -- inputs : clk, rst
  -- outputs: trigcounter, distcounter
  seq_sm : process (clk, rst) is
  begin  -- process seq_sm
    if rst = '0' then                   -- asynchronous reset (active low)
      present_state <= trigwait;
      next_state <= trigwait;
      trigcounter   <= (others => '0');
      distcounter   <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      echo_last     <= echo_synced;
      echo_synced   <= echo_unsynced;
      echo_unsynced <= echo;

      trig_last     <= trig_synced;
      trig_synced   <= trig_unsynced;
      trig_unsynced <= trig;

      present_state <= next_state;

      case (present_state) is
      when trigwait =>
        trigcounter <= (others => '0');
        if trig_last = '0' and trig_synced = '1' then
          distout <= (others => '0');
          distcounter <= (others => '0');
          ready <= '0';
          triglatch  <= '1';
          next_state <= trighold;
        end if;
      when trighold =>
        trigcounter <= std_logic_vector(unsigned(trigcounter)+1);
        if trigcounter >=  "1000011100" then
          triglatch <= '0';
          next_state <= echowait;
        end if;
      when echowait =>
        if echo_last = '0' and echo_synced = '1' then
          next_state <= echocount;
        end if;
      when echocount =>
        distcounter <= std_logic_vector(unsigned(distcounter)+1);
        if echo_last = '1' and echo_synced = '0' then
          next_state <= trigwait;
          distout <= distcounter;
          ready <= '1';
        end if;
      when others =>
    end case;
   end if;
  end process seq_sm;

end architecture behavorial;

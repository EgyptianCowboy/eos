// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Oct 21 21:32:07 2019
// Host        : t480s running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /home/sil/Documents/school/EOS/ex3/sr04/sr04.srcs/sources_1/bd/design_1/ip/design_1_HCSR04_2_0_2/design_1_HCSR04_2_0_2_sim_netlist.v
// Design      : design_1_HCSR04_2_0_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_HCSR04_2_0_2,HCSR04_2_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "HCSR04_2_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_HCSR04_2_0_2
   (echo,
    triglatch,
    rst,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input echo;
  output triglatch;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 rst RST" *) (* x_interface_parameter = "XIL_INTERFACENAME rst, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input rst;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn:rst, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire echo;
  wire rst;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  assign triglatch = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_HCSR04_2_0_2_HCSR04_2_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .echo(echo),
        .rst(rst),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "HCSR04_2_v1_0" *) 
module design_1_HCSR04_2_0_2_HCSR04_2_v1_0
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    rst,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready,
    echo);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input rst;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;
  input echo;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire echo;
  wire rst;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  design_1_HCSR04_2_0_2_HCSR04_2_v1_0_S00_AXI HCSR04_2_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .echo(echo),
        .rst(rst),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "HCSR04_2_v1_0_S00_AXI" *) 
module design_1_HCSR04_2_0_2_HCSR04_2_v1_0_S00_AXI
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    rst,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready,
    echo);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input rst;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;
  input echo;

  wire HCSR04Driver_n_1;
  wire HCSR04Driver_n_10;
  wire HCSR04Driver_n_11;
  wire HCSR04Driver_n_12;
  wire HCSR04Driver_n_13;
  wire HCSR04Driver_n_14;
  wire HCSR04Driver_n_15;
  wire HCSR04Driver_n_16;
  wire HCSR04Driver_n_17;
  wire HCSR04Driver_n_18;
  wire HCSR04Driver_n_19;
  wire HCSR04Driver_n_2;
  wire HCSR04Driver_n_20;
  wire HCSR04Driver_n_21;
  wire HCSR04Driver_n_22;
  wire HCSR04Driver_n_23;
  wire HCSR04Driver_n_24;
  wire HCSR04Driver_n_3;
  wire HCSR04Driver_n_4;
  wire HCSR04Driver_n_5;
  wire HCSR04Driver_n_6;
  wire HCSR04Driver_n_7;
  wire HCSR04Driver_n_8;
  wire HCSR04Driver_n_9;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire echo;
  wire [1:0]p_0_in;
  wire [31:7]p_1_in;
  wire ready;
  wire [31:0]reg_data_out;
  wire rst;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire slv_reg_rden;
  wire slv_reg_wren__2;

  design_1_HCSR04_2_0_2_sr04_fsm HCSR04Driver
       (.\distout_reg[0]_0 (HCSR04Driver_n_24),
        .\distout_reg[10]_0 (HCSR04Driver_n_14),
        .\distout_reg[11]_0 (HCSR04Driver_n_13),
        .\distout_reg[12]_0 (HCSR04Driver_n_12),
        .\distout_reg[13]_0 (HCSR04Driver_n_11),
        .\distout_reg[14]_0 (HCSR04Driver_n_10),
        .\distout_reg[15]_0 (HCSR04Driver_n_9),
        .\distout_reg[16]_0 (HCSR04Driver_n_8),
        .\distout_reg[17]_0 (HCSR04Driver_n_7),
        .\distout_reg[18]_0 (HCSR04Driver_n_6),
        .\distout_reg[19]_0 (HCSR04Driver_n_5),
        .\distout_reg[1]_0 (HCSR04Driver_n_23),
        .\distout_reg[20]_0 (HCSR04Driver_n_4),
        .\distout_reg[21]_0 (HCSR04Driver_n_3),
        .\distout_reg[22]_0 (HCSR04Driver_n_2),
        .\distout_reg[23]_0 (HCSR04Driver_n_1),
        .\distout_reg[2]_0 (HCSR04Driver_n_22),
        .\distout_reg[3]_0 (HCSR04Driver_n_21),
        .\distout_reg[4]_0 (HCSR04Driver_n_20),
        .\distout_reg[5]_0 (HCSR04Driver_n_19),
        .\distout_reg[6]_0 (HCSR04Driver_n_18),
        .\distout_reg[7]_0 (HCSR04Driver_n_17),
        .\distout_reg[8]_0 (HCSR04Driver_n_16),
        .\distout_reg[9]_0 (HCSR04Driver_n_15),
        .echo(echo),
        .ready(ready),
        .rst(rst),
        .s00_axi_aclk(s00_axi_aclk));
  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[0]_i_1 
       (.I0(slv_reg1[0]),
        .I1(HCSR04Driver_n_24),
        .I2(slv_reg3[0]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[0]),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg1[10]),
        .I1(HCSR04Driver_n_14),
        .I2(slv_reg3[10]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[10]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg1[11]),
        .I1(HCSR04Driver_n_13),
        .I2(slv_reg3[11]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[11]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg1[12]),
        .I1(HCSR04Driver_n_12),
        .I2(slv_reg3[12]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[12]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg1[13]),
        .I1(HCSR04Driver_n_11),
        .I2(slv_reg3[13]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[13]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg1[14]),
        .I1(HCSR04Driver_n_10),
        .I2(slv_reg3[14]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[14]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg1[15]),
        .I1(HCSR04Driver_n_9),
        .I2(slv_reg3[15]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[15]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[16]_i_1 
       (.I0(slv_reg1[16]),
        .I1(HCSR04Driver_n_8),
        .I2(slv_reg3[16]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[16]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[17]_i_1 
       (.I0(slv_reg1[17]),
        .I1(HCSR04Driver_n_7),
        .I2(slv_reg3[17]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[17]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[18]_i_1 
       (.I0(slv_reg1[18]),
        .I1(HCSR04Driver_n_6),
        .I2(slv_reg3[18]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[18]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[19]_i_1 
       (.I0(slv_reg1[19]),
        .I1(HCSR04Driver_n_5),
        .I2(slv_reg3[19]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[19]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[1]_i_1 
       (.I0(slv_reg1[1]),
        .I1(HCSR04Driver_n_23),
        .I2(slv_reg3[1]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[1]),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[20]_i_1 
       (.I0(slv_reg1[20]),
        .I1(HCSR04Driver_n_4),
        .I2(slv_reg3[20]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[20]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[21]_i_1 
       (.I0(slv_reg1[21]),
        .I1(HCSR04Driver_n_3),
        .I2(slv_reg3[21]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[21]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_reg1[22]),
        .I1(HCSR04Driver_n_2),
        .I2(slv_reg3[22]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[22]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[23]_i_1 
       (.I0(slv_reg1[23]),
        .I1(HCSR04Driver_n_1),
        .I2(slv_reg3[23]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[23]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg1[24]),
        .I1(ready),
        .I2(slv_reg3[24]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[24]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg1[25]),
        .I1(slv_reg3[25]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[25]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg1[26]),
        .I1(slv_reg3[26]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[26]),
        .O(reg_data_out[26]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg1[27]),
        .I1(slv_reg3[27]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[27]),
        .O(reg_data_out[27]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg1[28]),
        .I1(slv_reg3[28]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[28]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg1[29]),
        .I1(slv_reg3[29]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[29]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[2]_i_1 
       (.I0(slv_reg1[2]),
        .I1(HCSR04Driver_n_22),
        .I2(slv_reg3[2]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[2]),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[30]_i_1 
       (.I0(slv_reg1[30]),
        .I1(slv_reg3[30]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[30]),
        .O(reg_data_out[30]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg1[31]),
        .I1(slv_reg3[31]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[31]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[3]_i_1 
       (.I0(slv_reg1[3]),
        .I1(HCSR04Driver_n_21),
        .I2(slv_reg3[3]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[3]),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[4]_i_1 
       (.I0(slv_reg1[4]),
        .I1(HCSR04Driver_n_20),
        .I2(slv_reg3[4]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[4]),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[5]_i_1 
       (.I0(slv_reg1[5]),
        .I1(HCSR04Driver_n_19),
        .I2(slv_reg3[5]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[5]),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[6]_i_1 
       (.I0(slv_reg1[6]),
        .I1(HCSR04Driver_n_18),
        .I2(slv_reg3[6]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[6]),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[7]_i_1 
       (.I0(slv_reg1[7]),
        .I1(HCSR04Driver_n_17),
        .I2(slv_reg3[7]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[7]),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[8]_i_1 
       (.I0(slv_reg1[8]),
        .I1(HCSR04Driver_n_16),
        .I2(slv_reg3[8]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[8]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg1[9]),
        .I1(HCSR04Driver_n_15),
        .I2(slv_reg3[9]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[9]),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
endmodule

(* ORIG_REF_NAME = "sr04_fsm" *) 
module design_1_HCSR04_2_0_2_sr04_fsm
   (ready,
    \distout_reg[23]_0 ,
    \distout_reg[22]_0 ,
    \distout_reg[21]_0 ,
    \distout_reg[20]_0 ,
    \distout_reg[19]_0 ,
    \distout_reg[18]_0 ,
    \distout_reg[17]_0 ,
    \distout_reg[16]_0 ,
    \distout_reg[15]_0 ,
    \distout_reg[14]_0 ,
    \distout_reg[13]_0 ,
    \distout_reg[12]_0 ,
    \distout_reg[11]_0 ,
    \distout_reg[10]_0 ,
    \distout_reg[9]_0 ,
    \distout_reg[8]_0 ,
    \distout_reg[7]_0 ,
    \distout_reg[6]_0 ,
    \distout_reg[5]_0 ,
    \distout_reg[4]_0 ,
    \distout_reg[3]_0 ,
    \distout_reg[2]_0 ,
    \distout_reg[1]_0 ,
    \distout_reg[0]_0 ,
    s00_axi_aclk,
    rst,
    echo);
  output ready;
  output \distout_reg[23]_0 ;
  output \distout_reg[22]_0 ;
  output \distout_reg[21]_0 ;
  output \distout_reg[20]_0 ;
  output \distout_reg[19]_0 ;
  output \distout_reg[18]_0 ;
  output \distout_reg[17]_0 ;
  output \distout_reg[16]_0 ;
  output \distout_reg[15]_0 ;
  output \distout_reg[14]_0 ;
  output \distout_reg[13]_0 ;
  output \distout_reg[12]_0 ;
  output \distout_reg[11]_0 ;
  output \distout_reg[10]_0 ;
  output \distout_reg[9]_0 ;
  output \distout_reg[8]_0 ;
  output \distout_reg[7]_0 ;
  output \distout_reg[6]_0 ;
  output \distout_reg[5]_0 ;
  output \distout_reg[4]_0 ;
  output \distout_reg[3]_0 ;
  output \distout_reg[2]_0 ;
  output \distout_reg[1]_0 ;
  output \distout_reg[0]_0 ;
  input s00_axi_aclk;
  input rst;
  input echo;

  wire \FSM_sequential_next_state[0]_i_1_n_0 ;
  wire \FSM_sequential_next_state[1]_i_1_n_0 ;
  wire \FSM_sequential_next_state[1]_i_2_n_0 ;
  wire \FSM_sequential_next_state[1]_i_3_n_0 ;
  wire \FSM_sequential_next_state[1]_i_4_n_0 ;
  wire \FSM_sequential_present_state[1]_i_1_n_0 ;
  wire \FSM_sequential_present_state_reg_n_0_[0] ;
  wire \FSM_sequential_present_state_reg_n_0_[1] ;
  wire [0:0]distcounter;
  wire distcounter_0;
  wire \distcounter_reg[12]_i_1_n_0 ;
  wire \distcounter_reg[12]_i_1_n_1 ;
  wire \distcounter_reg[12]_i_1_n_2 ;
  wire \distcounter_reg[12]_i_1_n_3 ;
  wire \distcounter_reg[16]_i_1_n_0 ;
  wire \distcounter_reg[16]_i_1_n_1 ;
  wire \distcounter_reg[16]_i_1_n_2 ;
  wire \distcounter_reg[16]_i_1_n_3 ;
  wire \distcounter_reg[20]_i_1_n_0 ;
  wire \distcounter_reg[20]_i_1_n_1 ;
  wire \distcounter_reg[20]_i_1_n_2 ;
  wire \distcounter_reg[20]_i_1_n_3 ;
  wire \distcounter_reg[23]_i_2_n_2 ;
  wire \distcounter_reg[23]_i_2_n_3 ;
  wire \distcounter_reg[4]_i_1_n_0 ;
  wire \distcounter_reg[4]_i_1_n_1 ;
  wire \distcounter_reg[4]_i_1_n_2 ;
  wire \distcounter_reg[4]_i_1_n_3 ;
  wire \distcounter_reg[8]_i_1_n_0 ;
  wire \distcounter_reg[8]_i_1_n_1 ;
  wire \distcounter_reg[8]_i_1_n_2 ;
  wire \distcounter_reg[8]_i_1_n_3 ;
  wire \distcounter_reg_n_0_[0] ;
  wire \distcounter_reg_n_0_[10] ;
  wire \distcounter_reg_n_0_[11] ;
  wire \distcounter_reg_n_0_[12] ;
  wire \distcounter_reg_n_0_[13] ;
  wire \distcounter_reg_n_0_[14] ;
  wire \distcounter_reg_n_0_[15] ;
  wire \distcounter_reg_n_0_[16] ;
  wire \distcounter_reg_n_0_[17] ;
  wire \distcounter_reg_n_0_[18] ;
  wire \distcounter_reg_n_0_[19] ;
  wire \distcounter_reg_n_0_[1] ;
  wire \distcounter_reg_n_0_[20] ;
  wire \distcounter_reg_n_0_[21] ;
  wire \distcounter_reg_n_0_[22] ;
  wire \distcounter_reg_n_0_[23] ;
  wire \distcounter_reg_n_0_[2] ;
  wire \distcounter_reg_n_0_[3] ;
  wire \distcounter_reg_n_0_[4] ;
  wire \distcounter_reg_n_0_[5] ;
  wire \distcounter_reg_n_0_[6] ;
  wire \distcounter_reg_n_0_[7] ;
  wire \distcounter_reg_n_0_[8] ;
  wire \distcounter_reg_n_0_[9] ;
  wire \distout_reg[0]_0 ;
  wire \distout_reg[10]_0 ;
  wire \distout_reg[11]_0 ;
  wire \distout_reg[12]_0 ;
  wire \distout_reg[13]_0 ;
  wire \distout_reg[14]_0 ;
  wire \distout_reg[15]_0 ;
  wire \distout_reg[16]_0 ;
  wire \distout_reg[17]_0 ;
  wire \distout_reg[18]_0 ;
  wire \distout_reg[19]_0 ;
  wire \distout_reg[1]_0 ;
  wire \distout_reg[20]_0 ;
  wire \distout_reg[21]_0 ;
  wire \distout_reg[22]_0 ;
  wire \distout_reg[23]_0 ;
  wire \distout_reg[2]_0 ;
  wire \distout_reg[3]_0 ;
  wire \distout_reg[4]_0 ;
  wire \distout_reg[5]_0 ;
  wire \distout_reg[6]_0 ;
  wire \distout_reg[7]_0 ;
  wire \distout_reg[8]_0 ;
  wire \distout_reg[9]_0 ;
  wire echo;
  wire echo_last;
  wire echo_last_i_1_n_0;
  wire echo_synced;
  wire echo_synced_i_1_n_0;
  wire echo_unsynced;
  wire echo_unsynced_i_1_n_0;
  wire [23:1]in15;
  wire [11:1]in19;
  wire [1:0]next_state;
  wire ready;
  wire ready0;
  wire ready_i_1_n_0;
  wire rst;
  wire s00_axi_aclk;
  wire [11:0]trigcounter;
  wire trigcounter_1;
  wire \trigcounter_reg[11]_i_3_n_2 ;
  wire \trigcounter_reg[11]_i_3_n_3 ;
  wire \trigcounter_reg[4]_i_2_n_0 ;
  wire \trigcounter_reg[4]_i_2_n_1 ;
  wire \trigcounter_reg[4]_i_2_n_2 ;
  wire \trigcounter_reg[4]_i_2_n_3 ;
  wire \trigcounter_reg[8]_i_2_n_0 ;
  wire \trigcounter_reg[8]_i_2_n_1 ;
  wire \trigcounter_reg[8]_i_2_n_2 ;
  wire \trigcounter_reg[8]_i_2_n_3 ;
  wire \trigcounter_reg_n_0_[0] ;
  wire \trigcounter_reg_n_0_[10] ;
  wire \trigcounter_reg_n_0_[11] ;
  wire \trigcounter_reg_n_0_[1] ;
  wire \trigcounter_reg_n_0_[2] ;
  wire \trigcounter_reg_n_0_[3] ;
  wire \trigcounter_reg_n_0_[4] ;
  wire \trigcounter_reg_n_0_[5] ;
  wire \trigcounter_reg_n_0_[6] ;
  wire \trigcounter_reg_n_0_[7] ;
  wire \trigcounter_reg_n_0_[8] ;
  wire \trigcounter_reg_n_0_[9] ;
  wire [3:2]\NLW_distcounter_reg[23]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_distcounter_reg[23]_i_2_O_UNCONNECTED ;
  wire [3:2]\NLW_trigcounter_reg[11]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_trigcounter_reg[11]_i_3_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hA2AABAFF00003055)) 
    \FSM_sequential_next_state[0]_i_1 
       (.I0(\FSM_sequential_next_state[1]_i_2_n_0 ),
        .I1(echo_last),
        .I2(echo_synced),
        .I3(\FSM_sequential_present_state_reg_n_0_[1] ),
        .I4(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I5(next_state[0]),
        .O(\FSM_sequential_next_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA2FFBAAA00553000)) 
    \FSM_sequential_next_state[1]_i_1 
       (.I0(\FSM_sequential_next_state[1]_i_2_n_0 ),
        .I1(echo_last),
        .I2(echo_synced),
        .I3(\FSM_sequential_present_state_reg_n_0_[1] ),
        .I4(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I5(next_state[1]),
        .O(\FSM_sequential_next_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h777777777FFFFFFF)) 
    \FSM_sequential_next_state[1]_i_2 
       (.I0(\FSM_sequential_next_state[1]_i_3_n_0 ),
        .I1(\trigcounter_reg_n_0_[11] ),
        .I2(\trigcounter_reg_n_0_[4] ),
        .I3(\trigcounter_reg_n_0_[5] ),
        .I4(\trigcounter_reg_n_0_[6] ),
        .I5(\FSM_sequential_next_state[1]_i_4_n_0 ),
        .O(\FSM_sequential_next_state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_next_state[1]_i_3 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(\FSM_sequential_present_state_reg_n_0_[1] ),
        .O(\FSM_sequential_next_state[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_next_state[1]_i_4 
       (.I0(\trigcounter_reg_n_0_[9] ),
        .I1(\trigcounter_reg_n_0_[10] ),
        .I2(\trigcounter_reg_n_0_[7] ),
        .I3(\trigcounter_reg_n_0_[8] ),
        .O(\FSM_sequential_next_state[1]_i_4_n_0 ));
  FDCE \FSM_sequential_next_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(\FSM_sequential_next_state[0]_i_1_n_0 ),
        .Q(next_state[0]));
  FDCE \FSM_sequential_next_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(\FSM_sequential_next_state[1]_i_1_n_0 ),
        .Q(next_state[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_sequential_present_state[1]_i_1 
       (.I0(rst),
        .O(\FSM_sequential_present_state[1]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "trighold:01,echowait:10,trigwait:00,echocount:11" *) 
  FDCE \FSM_sequential_present_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(next_state[0]),
        .Q(\FSM_sequential_present_state_reg_n_0_[0] ));
  (* FSM_ENCODED_STATES = "trighold:01,echowait:10,trigwait:00,echocount:11" *) 
  FDCE \FSM_sequential_present_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(next_state[1]),
        .Q(\FSM_sequential_present_state_reg_n_0_[1] ));
  LUT1 #(
    .INIT(2'h1)) 
    \distcounter[0]_i_1 
       (.I0(\distcounter_reg_n_0_[0] ),
        .O(distcounter));
  LUT2 #(
    .INIT(4'h8)) 
    \distcounter[23]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[1] ),
        .I1(\FSM_sequential_present_state_reg_n_0_[0] ),
        .O(distcounter_0));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[0] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(distcounter),
        .Q(\distcounter_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[10] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[10]),
        .Q(\distcounter_reg_n_0_[10] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[11] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[11]),
        .Q(\distcounter_reg_n_0_[11] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[12] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[12]),
        .Q(\distcounter_reg_n_0_[12] ));
  CARRY4 \distcounter_reg[12]_i_1 
       (.CI(\distcounter_reg[8]_i_1_n_0 ),
        .CO({\distcounter_reg[12]_i_1_n_0 ,\distcounter_reg[12]_i_1_n_1 ,\distcounter_reg[12]_i_1_n_2 ,\distcounter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in15[12:9]),
        .S({\distcounter_reg_n_0_[12] ,\distcounter_reg_n_0_[11] ,\distcounter_reg_n_0_[10] ,\distcounter_reg_n_0_[9] }));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[13] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[13]),
        .Q(\distcounter_reg_n_0_[13] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[14] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[14]),
        .Q(\distcounter_reg_n_0_[14] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[15] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[15]),
        .Q(\distcounter_reg_n_0_[15] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[16] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[16]),
        .Q(\distcounter_reg_n_0_[16] ));
  CARRY4 \distcounter_reg[16]_i_1 
       (.CI(\distcounter_reg[12]_i_1_n_0 ),
        .CO({\distcounter_reg[16]_i_1_n_0 ,\distcounter_reg[16]_i_1_n_1 ,\distcounter_reg[16]_i_1_n_2 ,\distcounter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in15[16:13]),
        .S({\distcounter_reg_n_0_[16] ,\distcounter_reg_n_0_[15] ,\distcounter_reg_n_0_[14] ,\distcounter_reg_n_0_[13] }));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[17] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[17]),
        .Q(\distcounter_reg_n_0_[17] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[18] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[18]),
        .Q(\distcounter_reg_n_0_[18] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[19] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[19]),
        .Q(\distcounter_reg_n_0_[19] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[1] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[1]),
        .Q(\distcounter_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[20] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[20]),
        .Q(\distcounter_reg_n_0_[20] ));
  CARRY4 \distcounter_reg[20]_i_1 
       (.CI(\distcounter_reg[16]_i_1_n_0 ),
        .CO({\distcounter_reg[20]_i_1_n_0 ,\distcounter_reg[20]_i_1_n_1 ,\distcounter_reg[20]_i_1_n_2 ,\distcounter_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in15[20:17]),
        .S({\distcounter_reg_n_0_[20] ,\distcounter_reg_n_0_[19] ,\distcounter_reg_n_0_[18] ,\distcounter_reg_n_0_[17] }));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[21] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[21]),
        .Q(\distcounter_reg_n_0_[21] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[22] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[22]),
        .Q(\distcounter_reg_n_0_[22] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[23] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[23]),
        .Q(\distcounter_reg_n_0_[23] ));
  CARRY4 \distcounter_reg[23]_i_2 
       (.CI(\distcounter_reg[20]_i_1_n_0 ),
        .CO({\NLW_distcounter_reg[23]_i_2_CO_UNCONNECTED [3:2],\distcounter_reg[23]_i_2_n_2 ,\distcounter_reg[23]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_distcounter_reg[23]_i_2_O_UNCONNECTED [3],in15[23:21]}),
        .S({1'b0,\distcounter_reg_n_0_[23] ,\distcounter_reg_n_0_[22] ,\distcounter_reg_n_0_[21] }));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[2] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[2]),
        .Q(\distcounter_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[3] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[3]),
        .Q(\distcounter_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[4] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[4]),
        .Q(\distcounter_reg_n_0_[4] ));
  CARRY4 \distcounter_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\distcounter_reg[4]_i_1_n_0 ,\distcounter_reg[4]_i_1_n_1 ,\distcounter_reg[4]_i_1_n_2 ,\distcounter_reg[4]_i_1_n_3 }),
        .CYINIT(\distcounter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in15[4:1]),
        .S({\distcounter_reg_n_0_[4] ,\distcounter_reg_n_0_[3] ,\distcounter_reg_n_0_[2] ,\distcounter_reg_n_0_[1] }));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[5] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[5]),
        .Q(\distcounter_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[6] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[6]),
        .Q(\distcounter_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[7] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[7]),
        .Q(\distcounter_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[8] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[8]),
        .Q(\distcounter_reg_n_0_[8] ));
  CARRY4 \distcounter_reg[8]_i_1 
       (.CI(\distcounter_reg[4]_i_1_n_0 ),
        .CO({\distcounter_reg[8]_i_1_n_0 ,\distcounter_reg[8]_i_1_n_1 ,\distcounter_reg[8]_i_1_n_2 ,\distcounter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in15[8:5]),
        .S({\distcounter_reg_n_0_[8] ,\distcounter_reg_n_0_[7] ,\distcounter_reg_n_0_[6] ,\distcounter_reg_n_0_[5] }));
  FDCE #(
    .INIT(1'b0)) 
    \distcounter_reg[9] 
       (.C(s00_axi_aclk),
        .CE(distcounter_0),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(in15[9]),
        .Q(\distcounter_reg_n_0_[9] ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \distout[23]_i_1 
       (.I0(rst),
        .I1(echo_synced),
        .I2(echo_last),
        .I3(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I4(\FSM_sequential_present_state_reg_n_0_[1] ),
        .O(ready0));
  FDRE \distout_reg[0] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[0] ),
        .Q(\distout_reg[0]_0 ),
        .R(1'b0));
  FDRE \distout_reg[10] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[10] ),
        .Q(\distout_reg[10]_0 ),
        .R(1'b0));
  FDRE \distout_reg[11] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[11] ),
        .Q(\distout_reg[11]_0 ),
        .R(1'b0));
  FDRE \distout_reg[12] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[12] ),
        .Q(\distout_reg[12]_0 ),
        .R(1'b0));
  FDRE \distout_reg[13] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[13] ),
        .Q(\distout_reg[13]_0 ),
        .R(1'b0));
  FDRE \distout_reg[14] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[14] ),
        .Q(\distout_reg[14]_0 ),
        .R(1'b0));
  FDRE \distout_reg[15] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[15] ),
        .Q(\distout_reg[15]_0 ),
        .R(1'b0));
  FDRE \distout_reg[16] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[16] ),
        .Q(\distout_reg[16]_0 ),
        .R(1'b0));
  FDRE \distout_reg[17] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[17] ),
        .Q(\distout_reg[17]_0 ),
        .R(1'b0));
  FDRE \distout_reg[18] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[18] ),
        .Q(\distout_reg[18]_0 ),
        .R(1'b0));
  FDRE \distout_reg[19] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[19] ),
        .Q(\distout_reg[19]_0 ),
        .R(1'b0));
  FDRE \distout_reg[1] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[1] ),
        .Q(\distout_reg[1]_0 ),
        .R(1'b0));
  FDRE \distout_reg[20] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[20] ),
        .Q(\distout_reg[20]_0 ),
        .R(1'b0));
  FDRE \distout_reg[21] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[21] ),
        .Q(\distout_reg[21]_0 ),
        .R(1'b0));
  FDRE \distout_reg[22] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[22] ),
        .Q(\distout_reg[22]_0 ),
        .R(1'b0));
  FDRE \distout_reg[23] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[23] ),
        .Q(\distout_reg[23]_0 ),
        .R(1'b0));
  FDRE \distout_reg[2] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[2] ),
        .Q(\distout_reg[2]_0 ),
        .R(1'b0));
  FDRE \distout_reg[3] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[3] ),
        .Q(\distout_reg[3]_0 ),
        .R(1'b0));
  FDRE \distout_reg[4] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[4] ),
        .Q(\distout_reg[4]_0 ),
        .R(1'b0));
  FDRE \distout_reg[5] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[5] ),
        .Q(\distout_reg[5]_0 ),
        .R(1'b0));
  FDRE \distout_reg[6] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[6] ),
        .Q(\distout_reg[6]_0 ),
        .R(1'b0));
  FDRE \distout_reg[7] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[7] ),
        .Q(\distout_reg[7]_0 ),
        .R(1'b0));
  FDRE \distout_reg[8] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[8] ),
        .Q(\distout_reg[8]_0 ),
        .R(1'b0));
  FDRE \distout_reg[9] 
       (.C(s00_axi_aclk),
        .CE(ready0),
        .D(\distcounter_reg_n_0_[9] ),
        .Q(\distout_reg[9]_0 ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    echo_last_i_1
       (.I0(echo_synced),
        .I1(rst),
        .I2(echo_last),
        .O(echo_last_i_1_n_0));
  FDRE echo_last_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(echo_last_i_1_n_0),
        .Q(echo_last),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    echo_synced_i_1
       (.I0(echo_unsynced),
        .I1(rst),
        .I2(echo_synced),
        .O(echo_synced_i_1_n_0));
  FDRE echo_synced_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(echo_synced_i_1_n_0),
        .Q(echo_synced),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    echo_unsynced_i_1
       (.I0(echo),
        .I1(rst),
        .I2(echo_unsynced),
        .O(echo_unsynced_i_1_n_0));
  FDRE echo_unsynced_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(echo_unsynced_i_1_n_0),
        .Q(echo_unsynced),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF20000000)) 
    ready_i_1
       (.I0(rst),
        .I1(echo_synced),
        .I2(echo_last),
        .I3(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I4(\FSM_sequential_present_state_reg_n_0_[1] ),
        .I5(ready),
        .O(ready_i_1_n_0));
  FDRE ready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(ready_i_1_n_0),
        .Q(ready),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \trigcounter[0]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(\trigcounter_reg_n_0_[0] ),
        .O(trigcounter[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[10]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[10]),
        .O(trigcounter[10]));
  LUT1 #(
    .INIT(2'h1)) 
    \trigcounter[11]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[1] ),
        .O(trigcounter_1));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[11]_i_2 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[11]),
        .O(trigcounter[11]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[1]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[1]),
        .O(trigcounter[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[2]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[2]),
        .O(trigcounter[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[3]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[3]),
        .O(trigcounter[3]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[4]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[4]),
        .O(trigcounter[4]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[5]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[5]),
        .O(trigcounter[5]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[6]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[6]),
        .O(trigcounter[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[7]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[7]),
        .O(trigcounter[7]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[8]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[8]),
        .O(trigcounter[8]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \trigcounter[9]_i_1 
       (.I0(\FSM_sequential_present_state_reg_n_0_[0] ),
        .I1(in19[9]),
        .O(trigcounter[9]));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[0] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[0]),
        .Q(\trigcounter_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[10] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[10]),
        .Q(\trigcounter_reg_n_0_[10] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[11] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[11]),
        .Q(\trigcounter_reg_n_0_[11] ));
  CARRY4 \trigcounter_reg[11]_i_3 
       (.CI(\trigcounter_reg[8]_i_2_n_0 ),
        .CO({\NLW_trigcounter_reg[11]_i_3_CO_UNCONNECTED [3:2],\trigcounter_reg[11]_i_3_n_2 ,\trigcounter_reg[11]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trigcounter_reg[11]_i_3_O_UNCONNECTED [3],in19[11:9]}),
        .S({1'b0,\trigcounter_reg_n_0_[11] ,\trigcounter_reg_n_0_[10] ,\trigcounter_reg_n_0_[9] }));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[1] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[1]),
        .Q(\trigcounter_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[2] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[2]),
        .Q(\trigcounter_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[3] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[3]),
        .Q(\trigcounter_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[4] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[4]),
        .Q(\trigcounter_reg_n_0_[4] ));
  CARRY4 \trigcounter_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\trigcounter_reg[4]_i_2_n_0 ,\trigcounter_reg[4]_i_2_n_1 ,\trigcounter_reg[4]_i_2_n_2 ,\trigcounter_reg[4]_i_2_n_3 }),
        .CYINIT(\trigcounter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in19[4:1]),
        .S({\trigcounter_reg_n_0_[4] ,\trigcounter_reg_n_0_[3] ,\trigcounter_reg_n_0_[2] ,\trigcounter_reg_n_0_[1] }));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[5] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[5]),
        .Q(\trigcounter_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[6] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[6]),
        .Q(\trigcounter_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[7] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[7]),
        .Q(\trigcounter_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[8] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[8]),
        .Q(\trigcounter_reg_n_0_[8] ));
  CARRY4 \trigcounter_reg[8]_i_2 
       (.CI(\trigcounter_reg[4]_i_2_n_0 ),
        .CO({\trigcounter_reg[8]_i_2_n_0 ,\trigcounter_reg[8]_i_2_n_1 ,\trigcounter_reg[8]_i_2_n_2 ,\trigcounter_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in19[8:5]),
        .S({\trigcounter_reg_n_0_[8] ,\trigcounter_reg_n_0_[7] ,\trigcounter_reg_n_0_[6] ,\trigcounter_reg_n_0_[5] }));
  FDCE #(
    .INIT(1'b0)) 
    \trigcounter_reg[9] 
       (.C(s00_axi_aclk),
        .CE(trigcounter_1),
        .CLR(\FSM_sequential_present_state[1]_i_1_n_0 ),
        .D(trigcounter[9]),
        .Q(\trigcounter_reg_n_0_[9] ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

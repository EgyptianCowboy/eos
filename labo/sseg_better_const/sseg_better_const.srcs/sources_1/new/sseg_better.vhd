----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/18/2019 07:09:37 PM
-- Design Name: 
-- Module Name: sseg_better - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sseg_better is
    Port (invec: in std_logic_vector(3 downto 0);
          outseg: out std_logic_vector(7 downto 0));
end sseg_better;

architecture Behavioral of sseg_better is

begin
    with invec select
        outseg <=
        x"3F" when x"0",
        x"06" when x"1",
        x"5B" when x"2",
        x"4F" when x"3",
        x"66" when x"4",
        x"6D" when x"5",
        x"7D" when x"6",
        x"07" when x"7",
        x"7F" when x"8",
        x"6F" when x"9",
        x"77" when x"A",
        x"7C" when x"B",
        x"39" when x"C",
        x"5E" when x"D",
        x"79" when x"E",
        x"71" when x"F",
        x"80" when others;

end Behavioral;

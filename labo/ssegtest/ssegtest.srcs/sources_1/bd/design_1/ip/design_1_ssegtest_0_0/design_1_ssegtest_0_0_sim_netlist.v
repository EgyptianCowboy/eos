// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sun Nov  3 16:07:44 2019
// Host        : t480s running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /home/sil/Documents/school/EOS/labo/ssegtest/ssegtest.srcs/sources_1/bd/design_1/ip/design_1_ssegtest_0_0/design_1_ssegtest_0_0_sim_netlist.v
// Design      : design_1_ssegtest_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ssegtest_0_0,ssegtest,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "ssegtest,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_ssegtest_0_0
   (inVec,
    ssegout);
  input [3:0]inVec;
  output [7:0]ssegout;

  wire [3:0]inVec;
  wire [7:0]ssegout;

  design_1_ssegtest_0_0_ssegtest U0
       (.inVec(inVec),
        .ssegout(ssegout));
endmodule

(* ORIG_REF_NAME = "ssegtest" *) 
module design_1_ssegtest_0_0_ssegtest
   (ssegout,
    inVec);
  output [7:0]ssegout;
  input [3:0]inVec;

  wire [3:0]inVec;
  wire [7:0]ssegout;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h4177)) 
    \ssegout[0]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[1]),
        .I2(inVec[0]),
        .I3(inVec[2]),
        .O(ssegout[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFEF3)) 
    \ssegout[1]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[2]),
        .I2(inVec[1]),
        .I3(inVec[0]),
        .O(ssegout[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hB6FB)) 
    \ssegout[2]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[2]),
        .I2(inVec[1]),
        .I3(inVec[0]),
        .O(ssegout[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \ssegout[3]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[0]),
        .I2(inVec[1]),
        .I3(inVec[2]),
        .O(ssegout[3]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h2ECF)) 
    \ssegout[4]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[2]),
        .I2(inVec[1]),
        .I3(inVec[0]),
        .O(ssegout[4]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h284F)) 
    \ssegout[5]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[1]),
        .I2(inVec[2]),
        .I3(inVec[0]),
        .O(ssegout[5]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0820)) 
    \ssegout[6]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[0]),
        .I2(inVec[1]),
        .I3(inVec[2]),
        .O(ssegout[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hB77A)) 
    \ssegout[7]_INST_0 
       (.I0(inVec[3]),
        .I1(inVec[0]),
        .I2(inVec[2]),
        .I3(inVec[1]),
        .O(ssegout[7]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

set_property PACKAGE_PIN M15 [get_ports {ssegout[0]}];  # "M15.PMOD1_D0_N"
set_property PACKAGE_PIN L15 [get_ports {ssegout[1]}];  # "L15.PMOD1_D0_P"
set_property PACKAGE_PIN M14 [get_ports {ssegout[2]}];  # "M14.PMOD1_D1_N"
set_property PACKAGE_PIN L14 [get_ports {ssegout[3]}];  # "L14.PMOD1_D1_P"
set_property PACKAGE_PIN L13 [get_ports {ssegout[4]}];  # "L13.PMOD1_D2_N"
set_property PACKAGE_PIN K13 [get_ports {ssegout[5]}];  # "K13.PMOD1_D2_P"
set_property PACKAGE_PIN N14 [get_ports {ssegout[6]}];  # "N14.PMOD1_D3_N"
set_property PACKAGE_PIN N13 [get_ports {ssegout[7]}];  # "N13.PMOD1_D3_P"

set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssegout[7]}]

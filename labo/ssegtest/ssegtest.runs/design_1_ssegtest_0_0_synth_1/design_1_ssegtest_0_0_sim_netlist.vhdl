-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sun Nov  3 16:07:44 2019
-- Host        : t480s running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ssegtest_0_0_sim_netlist.vhdl
-- Design      : design_1_ssegtest_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z007sclg225-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ssegtest is
  port (
    ssegout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    inVec : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ssegtest;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ssegtest is
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ssegout[0]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ssegout[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ssegout[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ssegout[3]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ssegout[4]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ssegout[5]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ssegout[6]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ssegout[7]_INST_0\ : label is "soft_lutpair3";
begin
\ssegout[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4177"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(1),
      I2 => inVec(0),
      I3 => inVec(2),
      O => ssegout(0)
    );
\ssegout[1]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEF3"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(2),
      I2 => inVec(1),
      I3 => inVec(0),
      O => ssegout(1)
    );
\ssegout[2]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B6FB"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(2),
      I2 => inVec(1),
      I3 => inVec(0),
      O => ssegout(2)
    );
\ssegout[3]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(0),
      I2 => inVec(1),
      I3 => inVec(2),
      O => ssegout(3)
    );
\ssegout[4]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2ECF"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(2),
      I2 => inVec(1),
      I3 => inVec(0),
      O => ssegout(4)
    );
\ssegout[5]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"284F"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(1),
      I2 => inVec(2),
      I3 => inVec(0),
      O => ssegout(5)
    );
\ssegout[6]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0820"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(0),
      I2 => inVec(1),
      I3 => inVec(2),
      O => ssegout(6)
    );
\ssegout[7]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B77A"
    )
        port map (
      I0 => inVec(3),
      I1 => inVec(0),
      I2 => inVec(2),
      I3 => inVec(1),
      O => ssegout(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    inVec : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ssegout : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ssegtest_0_0,ssegtest,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ssegtest,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ssegtest
     port map (
      inVec(3 downto 0) => inVec(3 downto 0),
      ssegout(7 downto 0) => ssegout(7 downto 0)
    );
end STRUCTURE;

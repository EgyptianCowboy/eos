-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Fri Nov 15 21:16:01 2019
-- Host        : t480s running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_HCSR04_Final_0_0_sim_netlist.vhdl
-- Design      : design_1_HCSR04_Final_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z007sclg225-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srhc_fsm is
  port (
    trig : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 25 downto 0 );
    clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 25 downto 0 );
    \axi_rdata_reg[25]\ : in STD_LOGIC_VECTOR ( 25 downto 0 );
    axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[25]_0\ : in STD_LOGIC_VECTOR ( 25 downto 0 );
    echo : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srhc_fsm;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srhc_fsm is
  signal \FSM_sequential_state_n[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_n[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_n[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_n[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_n[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_n[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_n[1]_i_7_n_0\ : STD_LOGIC;
  signal dist_out : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \dist_out[23]_i_1_n_0\ : STD_LOGIC;
  signal dist_out_buffer : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \dist_out_buffer[10]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[11]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[12]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[13]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[14]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[15]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[16]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[17]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[18]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[19]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[1]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[20]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[21]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[22]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[23]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[23]_i_2_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[2]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[3]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[4]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[5]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[6]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[7]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[8]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer[9]_i_1_n_0\ : STD_LOGIC;
  signal \dist_out_buffer_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \dist_out_buffer_reg[12]_i_2_n_1\ : STD_LOGIC;
  signal \dist_out_buffer_reg[12]_i_2_n_2\ : STD_LOGIC;
  signal \dist_out_buffer_reg[12]_i_2_n_3\ : STD_LOGIC;
  signal \dist_out_buffer_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \dist_out_buffer_reg[16]_i_2_n_1\ : STD_LOGIC;
  signal \dist_out_buffer_reg[16]_i_2_n_2\ : STD_LOGIC;
  signal \dist_out_buffer_reg[16]_i_2_n_3\ : STD_LOGIC;
  signal \dist_out_buffer_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \dist_out_buffer_reg[20]_i_2_n_1\ : STD_LOGIC;
  signal \dist_out_buffer_reg[20]_i_2_n_2\ : STD_LOGIC;
  signal \dist_out_buffer_reg[20]_i_2_n_3\ : STD_LOGIC;
  signal \dist_out_buffer_reg[23]_i_3_n_2\ : STD_LOGIC;
  signal \dist_out_buffer_reg[23]_i_3_n_3\ : STD_LOGIC;
  signal \dist_out_buffer_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \dist_out_buffer_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \dist_out_buffer_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \dist_out_buffer_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \dist_out_buffer_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \dist_out_buffer_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \dist_out_buffer_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \dist_out_buffer_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[0]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[10]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[11]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[12]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[13]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[14]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[15]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[16]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[17]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[18]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[19]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[1]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[20]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[21]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[22]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[23]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[2]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[3]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[4]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[5]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[6]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[7]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[8]\ : STD_LOGIC;
  signal \dist_out_buffer_reg_n_0_[9]\ : STD_LOGIC;
  signal in3 : STD_LOGIC_VECTOR ( 23 downto 1 );
  signal oob : STD_LOGIC;
  signal oob_buffer : STD_LOGIC;
  signal \oob_buffer0__15\ : STD_LOGIC;
  signal \oob_buffer1__0\ : STD_LOGIC;
  signal oob_buffer_i_1_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_1_in3_in : STD_LOGIC;
  signal ready : STD_LOGIC;
  signal ready_buffer : STD_LOGIC;
  signal \ready_buffer1__0\ : STD_LOGIC;
  signal ready_buffer_i_1_n_0 : STD_LOGIC;
  signal \resync_echo_reg_n_0_[2]\ : STD_LOGIC;
  signal \resync_start[1]_i_1_n_0\ : STD_LOGIC;
  signal \resync_start_reg_n_0_[2]\ : STD_LOGIC;
  signal state_n : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal state_n1 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal trig_buffer : STD_LOGIC;
  signal trig_buffer_i_1_n_0 : STD_LOGIC;
  signal trig_buffer_i_2_n_0 : STD_LOGIC;
  signal trig_buffer_i_3_n_0 : STD_LOGIC;
  signal trig_buffer_i_4_n_0 : STD_LOGIC;
  signal trig_buffer_i_5_n_0 : STD_LOGIC;
  signal trig_buffer_i_6_n_0 : STD_LOGIC;
  signal trig_buffer_i_7_n_0 : STD_LOGIC;
  signal trig_buffer_i_8_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_10_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_10_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_10_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_10_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_11_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_11_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_12_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_12_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_12_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_12_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_13_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_13_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_13_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_13_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_14_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_14_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_14_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_14_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_15_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_15_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_15_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_15_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_16_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_16_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_16_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_16_n_3 : STD_LOGIC;
  signal trig_buffer_reg_i_9_n_0 : STD_LOGIC;
  signal trig_buffer_reg_i_9_n_1 : STD_LOGIC;
  signal trig_buffer_reg_i_9_n_2 : STD_LOGIC;
  signal trig_buffer_reg_i_9_n_3 : STD_LOGIC;
  signal \trig_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \trig_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \trig_count[12]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[12]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[12]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[12]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[16]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[16]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[16]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[16]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[20]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[20]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[20]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[20]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[24]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[24]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[24]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[24]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[28]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[28]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[28]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[28]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[4]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[4]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[4]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[4]_i_5_n_0\ : STD_LOGIC;
  signal \trig_count[8]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count[8]_i_3_n_0\ : STD_LOGIC;
  signal \trig_count[8]_i_4_n_0\ : STD_LOGIC;
  signal \trig_count[8]_i_5_n_0\ : STD_LOGIC;
  signal trig_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \trig_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \trig_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \trig_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_dist_out_buffer_reg[23]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_dist_out_buffer_reg[23]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_trig_buffer_reg_i_11_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_trig_buffer_reg_i_11_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_trig_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_state_n[1]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_sequential_state_n[1]_i_5\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_sequential_state_n[1]_i_6\ : label is "soft_lutpair3";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_n_reg[0]\ : label is "send_trig:01,wait_re_echo:10,wait_start:00,wait_fe_echo:11";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_n_reg[1]\ : label is "send_trig:01,wait_re_echo:10,wait_start:00,wait_fe_echo:11";
  attribute SOFT_HLUTNM of \dist_out_buffer[0]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \dist_out_buffer[10]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \dist_out_buffer[11]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \dist_out_buffer[12]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \dist_out_buffer[13]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \dist_out_buffer[14]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \dist_out_buffer[15]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \dist_out_buffer[16]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \dist_out_buffer[17]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \dist_out_buffer[18]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \dist_out_buffer[19]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \dist_out_buffer[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \dist_out_buffer[20]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dist_out_buffer[21]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \dist_out_buffer[22]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \dist_out_buffer[23]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dist_out_buffer[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \dist_out_buffer[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \dist_out_buffer[4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dist_out_buffer[5]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \dist_out_buffer[6]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \dist_out_buffer[7]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dist_out_buffer[8]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \dist_out_buffer[9]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of oob_buffer_i_3 : label is "soft_lutpair0";
begin
\FSM_sequential_state_n[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"110155550F00AAAA"
    )
        port map (
      I0 => \FSM_sequential_state_n[1]_i_2_n_0\,
      I1 => \oob_buffer0__15\,
      I2 => \resync_echo_reg_n_0_[2]\,
      I3 => p_1_in,
      I4 => state_n(1),
      I5 => state_n(0),
      O => \FSM_sequential_state_n[0]_i_1_n_0\
    );
\FSM_sequential_state_n[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1101AAAA5F550000"
    )
        port map (
      I0 => \FSM_sequential_state_n[1]_i_2_n_0\,
      I1 => \oob_buffer0__15\,
      I2 => \resync_echo_reg_n_0_[2]\,
      I3 => p_1_in,
      I4 => state_n(1),
      I5 => state_n(0),
      O => \FSM_sequential_state_n[1]_i_1_n_0\
    );
\FSM_sequential_state_n[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00F20002"
    )
        port map (
      I0 => p_1_in3_in,
      I1 => \resync_start_reg_n_0_[2]\,
      I2 => state_n(0),
      I3 => state_n(1),
      I4 => trig_buffer_i_2_n_0,
      O => \FSM_sequential_state_n[1]_i_2_n_0\
    );
\FSM_sequential_state_n[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEEEEEEFE"
    )
        port map (
      I0 => in3(22),
      I1 => in3(21),
      I2 => in3(17),
      I3 => \FSM_sequential_state_n[1]_i_4_n_0\,
      I4 => \FSM_sequential_state_n[1]_i_5_n_0\,
      I5 => in3(23),
      O => \oob_buffer0__15\
    );
\FSM_sequential_state_n[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555557777777F"
    )
        port map (
      I0 => in3(20),
      I1 => in3(15),
      I2 => \FSM_sequential_state_n[1]_i_6_n_0\,
      I3 => \FSM_sequential_state_n[1]_i_7_n_0\,
      I4 => in3(11),
      I5 => in3(16),
      O => \FSM_sequential_state_n[1]_i_4_n_0\
    );
\FSM_sequential_state_n[1]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => in3(18),
      I1 => in3(19),
      O => \FSM_sequential_state_n[1]_i_5_n_0\
    );
\FSM_sequential_state_n[1]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => in3(12),
      I1 => in3(13),
      O => \FSM_sequential_state_n[1]_i_6_n_0\
    );
\FSM_sequential_state_n[1]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEAAAA"
    )
        port map (
      I0 => in3(14),
      I1 => in3(8),
      I2 => in3(7),
      I3 => in3(9),
      I4 => in3(10),
      O => \FSM_sequential_state_n[1]_i_7_n_0\
    );
\FSM_sequential_state_n_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => \FSM_sequential_state_n[0]_i_1_n_0\,
      Q => state_n(0)
    );
\FSM_sequential_state_n_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => \FSM_sequential_state_n[1]_i_1_n_0\,
      Q => state_n(1)
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(0),
      I1 => dist_out(0),
      I2 => \axi_rdata_reg[25]\(0),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(0),
      O => D(0)
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(10),
      I1 => dist_out(10),
      I2 => \axi_rdata_reg[25]\(10),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(10),
      O => D(10)
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(11),
      I1 => dist_out(11),
      I2 => \axi_rdata_reg[25]\(11),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(11),
      O => D(11)
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(12),
      I1 => dist_out(12),
      I2 => \axi_rdata_reg[25]\(12),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(12),
      O => D(12)
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(13),
      I1 => dist_out(13),
      I2 => \axi_rdata_reg[25]\(13),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(13),
      O => D(13)
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(14),
      I1 => dist_out(14),
      I2 => \axi_rdata_reg[25]\(14),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(14),
      O => D(14)
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(15),
      I1 => dist_out(15),
      I2 => \axi_rdata_reg[25]\(15),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(15),
      O => D(15)
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(16),
      I1 => dist_out(16),
      I2 => \axi_rdata_reg[25]\(16),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(16),
      O => D(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(17),
      I1 => dist_out(17),
      I2 => \axi_rdata_reg[25]\(17),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(17),
      O => D(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(18),
      I1 => dist_out(18),
      I2 => \axi_rdata_reg[25]\(18),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(18),
      O => D(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(19),
      I1 => dist_out(19),
      I2 => \axi_rdata_reg[25]\(19),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(19),
      O => D(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(1),
      I1 => dist_out(1),
      I2 => \axi_rdata_reg[25]\(1),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(1),
      O => D(1)
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(20),
      I1 => dist_out(20),
      I2 => \axi_rdata_reg[25]\(20),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(20),
      O => D(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(21),
      I1 => dist_out(21),
      I2 => \axi_rdata_reg[25]\(21),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(21),
      O => D(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(22),
      I1 => dist_out(22),
      I2 => \axi_rdata_reg[25]\(22),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(22),
      O => D(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(23),
      I1 => dist_out(23),
      I2 => \axi_rdata_reg[25]\(23),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(23),
      O => D(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(24),
      I1 => ready,
      I2 => \axi_rdata_reg[25]\(24),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(24),
      O => D(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(25),
      I1 => oob,
      I2 => \axi_rdata_reg[25]\(25),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(25),
      O => D(25)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(2),
      I1 => dist_out(2),
      I2 => \axi_rdata_reg[25]\(2),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(2),
      O => D(2)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(3),
      I1 => dist_out(3),
      I2 => \axi_rdata_reg[25]\(3),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(3),
      O => D(3)
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(4),
      I1 => dist_out(4),
      I2 => \axi_rdata_reg[25]\(4),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(4),
      O => D(4)
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(5),
      I1 => dist_out(5),
      I2 => \axi_rdata_reg[25]\(5),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(5),
      O => D(5)
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(6),
      I1 => dist_out(6),
      I2 => \axi_rdata_reg[25]\(6),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(6),
      O => D(6)
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(7),
      I1 => dist_out(7),
      I2 => \axi_rdata_reg[25]\(7),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(7),
      O => D(7)
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(8),
      I1 => dist_out(8),
      I2 => \axi_rdata_reg[25]\(8),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(8),
      O => D(8)
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AAFFCCF0AA00CC"
    )
        port map (
      I0 => Q(9),
      I1 => dist_out(9),
      I2 => \axi_rdata_reg[25]\(9),
      I3 => axi_araddr(1),
      I4 => axi_araddr(0),
      I5 => \axi_rdata_reg[25]_0\(9),
      O => D(9)
    );
\dist_out[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => state_n(1),
      I1 => state_n(0),
      I2 => Q(1),
      O => \dist_out[23]_i_1_n_0\
    );
\dist_out_buffer[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => state_n(1),
      I1 => \dist_out_buffer_reg_n_0_[0]\,
      O => dist_out_buffer(0)
    );
\dist_out_buffer[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(10),
      O => \dist_out_buffer[10]_i_1_n_0\
    );
\dist_out_buffer[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(11),
      O => \dist_out_buffer[11]_i_1_n_0\
    );
\dist_out_buffer[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(12),
      O => \dist_out_buffer[12]_i_1_n_0\
    );
\dist_out_buffer[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(13),
      O => \dist_out_buffer[13]_i_1_n_0\
    );
\dist_out_buffer[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(14),
      O => \dist_out_buffer[14]_i_1_n_0\
    );
\dist_out_buffer[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(15),
      O => \dist_out_buffer[15]_i_1_n_0\
    );
\dist_out_buffer[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(16),
      O => \dist_out_buffer[16]_i_1_n_0\
    );
\dist_out_buffer[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(17),
      O => \dist_out_buffer[17]_i_1_n_0\
    );
\dist_out_buffer[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(18),
      O => \dist_out_buffer[18]_i_1_n_0\
    );
\dist_out_buffer[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(19),
      O => \dist_out_buffer[19]_i_1_n_0\
    );
\dist_out_buffer[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(1),
      O => \dist_out_buffer[1]_i_1_n_0\
    );
\dist_out_buffer[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(20),
      O => \dist_out_buffer[20]_i_1_n_0\
    );
\dist_out_buffer[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(21),
      O => \dist_out_buffer[21]_i_1_n_0\
    );
\dist_out_buffer[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(22),
      O => \dist_out_buffer[22]_i_1_n_0\
    );
\dist_out_buffer[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA10"
    )
        port map (
      I0 => state_n(0),
      I1 => \resync_start_reg_n_0_[2]\,
      I2 => p_1_in3_in,
      I3 => state_n(1),
      O => \dist_out_buffer[23]_i_1_n_0\
    );
\dist_out_buffer[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(23),
      O => \dist_out_buffer[23]_i_2_n_0\
    );
\dist_out_buffer[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(2),
      O => \dist_out_buffer[2]_i_1_n_0\
    );
\dist_out_buffer[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(3),
      O => \dist_out_buffer[3]_i_1_n_0\
    );
\dist_out_buffer[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(4),
      O => \dist_out_buffer[4]_i_1_n_0\
    );
\dist_out_buffer[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(5),
      O => \dist_out_buffer[5]_i_1_n_0\
    );
\dist_out_buffer[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(6),
      O => \dist_out_buffer[6]_i_1_n_0\
    );
\dist_out_buffer[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(7),
      O => \dist_out_buffer[7]_i_1_n_0\
    );
\dist_out_buffer[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(8),
      O => \dist_out_buffer[8]_i_1_n_0\
    );
\dist_out_buffer[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state_n(1),
      I1 => in3(9),
      O => \dist_out_buffer[9]_i_1_n_0\
    );
\dist_out_buffer_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => dist_out_buffer(0),
      Q => \dist_out_buffer_reg_n_0_[0]\
    );
\dist_out_buffer_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[10]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[10]\
    );
\dist_out_buffer_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[11]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[11]\
    );
\dist_out_buffer_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[12]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[12]\
    );
\dist_out_buffer_reg[12]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \dist_out_buffer_reg[8]_i_2_n_0\,
      CO(3) => \dist_out_buffer_reg[12]_i_2_n_0\,
      CO(2) => \dist_out_buffer_reg[12]_i_2_n_1\,
      CO(1) => \dist_out_buffer_reg[12]_i_2_n_2\,
      CO(0) => \dist_out_buffer_reg[12]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in3(12 downto 9),
      S(3) => \dist_out_buffer_reg_n_0_[12]\,
      S(2) => \dist_out_buffer_reg_n_0_[11]\,
      S(1) => \dist_out_buffer_reg_n_0_[10]\,
      S(0) => \dist_out_buffer_reg_n_0_[9]\
    );
\dist_out_buffer_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[13]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[13]\
    );
\dist_out_buffer_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[14]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[14]\
    );
\dist_out_buffer_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[15]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[15]\
    );
\dist_out_buffer_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[16]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[16]\
    );
\dist_out_buffer_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \dist_out_buffer_reg[12]_i_2_n_0\,
      CO(3) => \dist_out_buffer_reg[16]_i_2_n_0\,
      CO(2) => \dist_out_buffer_reg[16]_i_2_n_1\,
      CO(1) => \dist_out_buffer_reg[16]_i_2_n_2\,
      CO(0) => \dist_out_buffer_reg[16]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in3(16 downto 13),
      S(3) => \dist_out_buffer_reg_n_0_[16]\,
      S(2) => \dist_out_buffer_reg_n_0_[15]\,
      S(1) => \dist_out_buffer_reg_n_0_[14]\,
      S(0) => \dist_out_buffer_reg_n_0_[13]\
    );
\dist_out_buffer_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[17]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[17]\
    );
\dist_out_buffer_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[18]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[18]\
    );
\dist_out_buffer_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[19]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[19]\
    );
\dist_out_buffer_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[1]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[1]\
    );
\dist_out_buffer_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[20]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[20]\
    );
\dist_out_buffer_reg[20]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \dist_out_buffer_reg[16]_i_2_n_0\,
      CO(3) => \dist_out_buffer_reg[20]_i_2_n_0\,
      CO(2) => \dist_out_buffer_reg[20]_i_2_n_1\,
      CO(1) => \dist_out_buffer_reg[20]_i_2_n_2\,
      CO(0) => \dist_out_buffer_reg[20]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in3(20 downto 17),
      S(3) => \dist_out_buffer_reg_n_0_[20]\,
      S(2) => \dist_out_buffer_reg_n_0_[19]\,
      S(1) => \dist_out_buffer_reg_n_0_[18]\,
      S(0) => \dist_out_buffer_reg_n_0_[17]\
    );
\dist_out_buffer_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[21]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[21]\
    );
\dist_out_buffer_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[22]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[22]\
    );
\dist_out_buffer_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[23]_i_2_n_0\,
      Q => \dist_out_buffer_reg_n_0_[23]\
    );
\dist_out_buffer_reg[23]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \dist_out_buffer_reg[20]_i_2_n_0\,
      CO(3 downto 2) => \NLW_dist_out_buffer_reg[23]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \dist_out_buffer_reg[23]_i_3_n_2\,
      CO(0) => \dist_out_buffer_reg[23]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_dist_out_buffer_reg[23]_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => in3(23 downto 21),
      S(3) => '0',
      S(2) => \dist_out_buffer_reg_n_0_[23]\,
      S(1) => \dist_out_buffer_reg_n_0_[22]\,
      S(0) => \dist_out_buffer_reg_n_0_[21]\
    );
\dist_out_buffer_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[2]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[2]\
    );
\dist_out_buffer_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[3]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[3]\
    );
\dist_out_buffer_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[4]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[4]\
    );
\dist_out_buffer_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \dist_out_buffer_reg[4]_i_2_n_0\,
      CO(2) => \dist_out_buffer_reg[4]_i_2_n_1\,
      CO(1) => \dist_out_buffer_reg[4]_i_2_n_2\,
      CO(0) => \dist_out_buffer_reg[4]_i_2_n_3\,
      CYINIT => \dist_out_buffer_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in3(4 downto 1),
      S(3) => \dist_out_buffer_reg_n_0_[4]\,
      S(2) => \dist_out_buffer_reg_n_0_[3]\,
      S(1) => \dist_out_buffer_reg_n_0_[2]\,
      S(0) => \dist_out_buffer_reg_n_0_[1]\
    );
\dist_out_buffer_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[5]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[5]\
    );
\dist_out_buffer_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[6]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[6]\
    );
\dist_out_buffer_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[7]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[7]\
    );
\dist_out_buffer_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[8]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[8]\
    );
\dist_out_buffer_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \dist_out_buffer_reg[4]_i_2_n_0\,
      CO(3) => \dist_out_buffer_reg[8]_i_2_n_0\,
      CO(2) => \dist_out_buffer_reg[8]_i_2_n_1\,
      CO(1) => \dist_out_buffer_reg[8]_i_2_n_2\,
      CO(0) => \dist_out_buffer_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in3(8 downto 5),
      S(3) => \dist_out_buffer_reg_n_0_[8]\,
      S(2) => \dist_out_buffer_reg_n_0_[7]\,
      S(1) => \dist_out_buffer_reg_n_0_[6]\,
      S(0) => \dist_out_buffer_reg_n_0_[5]\
    );
\dist_out_buffer_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \dist_out_buffer[23]_i_1_n_0\,
      CLR => Q(1),
      D => \dist_out_buffer[9]_i_1_n_0\,
      Q => \dist_out_buffer_reg_n_0_[9]\
    );
\dist_out_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[0]\,
      Q => dist_out(0),
      R => '0'
    );
\dist_out_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[10]\,
      Q => dist_out(10),
      R => '0'
    );
\dist_out_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[11]\,
      Q => dist_out(11),
      R => '0'
    );
\dist_out_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[12]\,
      Q => dist_out(12),
      R => '0'
    );
\dist_out_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[13]\,
      Q => dist_out(13),
      R => '0'
    );
\dist_out_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[14]\,
      Q => dist_out(14),
      R => '0'
    );
\dist_out_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[15]\,
      Q => dist_out(15),
      R => '0'
    );
\dist_out_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[16]\,
      Q => dist_out(16),
      R => '0'
    );
\dist_out_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[17]\,
      Q => dist_out(17),
      R => '0'
    );
\dist_out_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[18]\,
      Q => dist_out(18),
      R => '0'
    );
\dist_out_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[19]\,
      Q => dist_out(19),
      R => '0'
    );
\dist_out_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[1]\,
      Q => dist_out(1),
      R => '0'
    );
\dist_out_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[20]\,
      Q => dist_out(20),
      R => '0'
    );
\dist_out_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[21]\,
      Q => dist_out(21),
      R => '0'
    );
\dist_out_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[22]\,
      Q => dist_out(22),
      R => '0'
    );
\dist_out_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[23]\,
      Q => dist_out(23),
      R => '0'
    );
\dist_out_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[2]\,
      Q => dist_out(2),
      R => '0'
    );
\dist_out_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[3]\,
      Q => dist_out(3),
      R => '0'
    );
\dist_out_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[4]\,
      Q => dist_out(4),
      R => '0'
    );
\dist_out_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[5]\,
      Q => dist_out(5),
      R => '0'
    );
\dist_out_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[6]\,
      Q => dist_out(6),
      R => '0'
    );
\dist_out_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[7]\,
      Q => dist_out(7),
      R => '0'
    );
\dist_out_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[8]\,
      Q => dist_out(8),
      R => '0'
    );
\dist_out_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \dist_out[23]_i_1_n_0\,
      D => \dist_out_buffer_reg_n_0_[9]\,
      Q => dist_out(9),
      R => '0'
    );
oob_buffer_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF0FF40400000"
    )
        port map (
      I0 => \oob_buffer1__0\,
      I1 => \oob_buffer0__15\,
      I2 => state_n(0),
      I3 => \ready_buffer1__0\,
      I4 => state_n(1),
      I5 => oob_buffer,
      O => oob_buffer_i_1_n_0
    );
oob_buffer_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \resync_echo_reg_n_0_[2]\,
      I1 => p_1_in,
      O => \oob_buffer1__0\
    );
oob_buffer_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_1_in3_in,
      I1 => \resync_start_reg_n_0_[2]\,
      O => \ready_buffer1__0\
    );
oob_buffer_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => oob_buffer_i_1_n_0,
      Q => oob_buffer
    );
oob_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => oob_buffer,
      Q => oob
    );
ready_buffer_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB000B"
    )
        port map (
      I0 => \resync_start_reg_n_0_[2]\,
      I1 => p_1_in3_in,
      I2 => state_n(0),
      I3 => state_n(1),
      I4 => ready_buffer,
      O => ready_buffer_i_1_n_0
    );
ready_buffer_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => ready_buffer_i_1_n_0,
      Q => ready_buffer
    );
ready_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => ready_buffer,
      Q => ready
    );
\resync_echo_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \resync_start[1]_i_1_n_0\,
      D => echo,
      Q => p_1_in,
      R => '0'
    );
\resync_echo_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \resync_start[1]_i_1_n_0\,
      D => p_1_in,
      Q => \resync_echo_reg_n_0_[2]\,
      R => '0'
    );
\resync_start[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => \resync_start[1]_i_1_n_0\
    );
\resync_start_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \resync_start[1]_i_1_n_0\,
      D => Q(0),
      Q => p_1_in3_in,
      R => '0'
    );
\resync_start_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \resync_start[1]_i_1_n_0\,
      D => p_1_in3_in,
      Q => \resync_start_reg_n_0_[2]\,
      R => '0'
    );
trig_buffer_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0FFF00000404"
    )
        port map (
      I0 => \resync_start_reg_n_0_[2]\,
      I1 => p_1_in3_in,
      I2 => state_n(0),
      I3 => trig_buffer_i_2_n_0,
      I4 => state_n(1),
      I5 => trig_buffer,
      O => trig_buffer_i_1_n_0
    );
trig_buffer_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => trig_buffer_i_3_n_0,
      I1 => trig_buffer_i_4_n_0,
      I2 => trig_buffer_i_5_n_0,
      I3 => trig_buffer_i_6_n_0,
      I4 => trig_buffer_i_7_n_0,
      I5 => trig_buffer_i_8_n_0,
      O => trig_buffer_i_2_n_0
    );
trig_buffer_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => state_n1(24),
      I1 => state_n1(25),
      I2 => state_n1(26),
      I3 => state_n1(27),
      I4 => state_n1(28),
      I5 => state_n1(29),
      O => trig_buffer_i_3_n_0
    );
trig_buffer_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF7F"
    )
        port map (
      I0 => state_n1(7),
      I1 => state_n1(8),
      I2 => state_n1(6),
      I3 => state_n1(9),
      I4 => state_n1(10),
      I5 => state_n1(11),
      O => trig_buffer_i_4_n_0
    );
trig_buffer_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => state_n1(18),
      I1 => state_n1(19),
      I2 => state_n1(20),
      I3 => state_n1(21),
      I4 => state_n1(22),
      I5 => state_n1(23),
      O => trig_buffer_i_5_n_0
    );
trig_buffer_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => state_n1(12),
      I1 => state_n1(13),
      I2 => state_n1(14),
      I3 => state_n1(15),
      I4 => state_n1(16),
      I5 => state_n1(17),
      O => trig_buffer_i_6_n_0
    );
trig_buffer_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => state_n1(30),
      I1 => state_n1(31),
      O => trig_buffer_i_7_n_0
    );
trig_buffer_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => state_n1(2),
      I1 => state_n1(1),
      I2 => trig_count_reg(0),
      I3 => state_n1(3),
      I4 => state_n1(4),
      I5 => state_n1(5),
      O => trig_buffer_i_8_n_0
    );
trig_buffer_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => trig_buffer_i_1_n_0,
      Q => trig_buffer
    );
trig_buffer_reg_i_10: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_9_n_0,
      CO(3) => trig_buffer_reg_i_10_n_0,
      CO(2) => trig_buffer_reg_i_10_n_1,
      CO(1) => trig_buffer_reg_i_10_n_2,
      CO(0) => trig_buffer_reg_i_10_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(28 downto 25),
      S(3 downto 0) => trig_count_reg(28 downto 25)
    );
trig_buffer_reg_i_11: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_10_n_0,
      CO(3 downto 2) => NLW_trig_buffer_reg_i_11_CO_UNCONNECTED(3 downto 2),
      CO(1) => trig_buffer_reg_i_11_n_2,
      CO(0) => trig_buffer_reg_i_11_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => NLW_trig_buffer_reg_i_11_O_UNCONNECTED(3),
      O(2 downto 0) => state_n1(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => trig_count_reg(31 downto 29)
    );
trig_buffer_reg_i_12: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_16_n_0,
      CO(3) => trig_buffer_reg_i_12_n_0,
      CO(2) => trig_buffer_reg_i_12_n_1,
      CO(1) => trig_buffer_reg_i_12_n_2,
      CO(0) => trig_buffer_reg_i_12_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(8 downto 5),
      S(3 downto 0) => trig_count_reg(8 downto 5)
    );
trig_buffer_reg_i_13: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_12_n_0,
      CO(3) => trig_buffer_reg_i_13_n_0,
      CO(2) => trig_buffer_reg_i_13_n_1,
      CO(1) => trig_buffer_reg_i_13_n_2,
      CO(0) => trig_buffer_reg_i_13_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(12 downto 9),
      S(3 downto 0) => trig_count_reg(12 downto 9)
    );
trig_buffer_reg_i_14: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_15_n_0,
      CO(3) => trig_buffer_reg_i_14_n_0,
      CO(2) => trig_buffer_reg_i_14_n_1,
      CO(1) => trig_buffer_reg_i_14_n_2,
      CO(0) => trig_buffer_reg_i_14_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(20 downto 17),
      S(3 downto 0) => trig_count_reg(20 downto 17)
    );
trig_buffer_reg_i_15: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_13_n_0,
      CO(3) => trig_buffer_reg_i_15_n_0,
      CO(2) => trig_buffer_reg_i_15_n_1,
      CO(1) => trig_buffer_reg_i_15_n_2,
      CO(0) => trig_buffer_reg_i_15_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(16 downto 13),
      S(3 downto 0) => trig_count_reg(16 downto 13)
    );
trig_buffer_reg_i_16: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => trig_buffer_reg_i_16_n_0,
      CO(2) => trig_buffer_reg_i_16_n_1,
      CO(1) => trig_buffer_reg_i_16_n_2,
      CO(0) => trig_buffer_reg_i_16_n_3,
      CYINIT => trig_count_reg(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(4 downto 1),
      S(3 downto 0) => trig_count_reg(4 downto 1)
    );
trig_buffer_reg_i_9: unisim.vcomponents.CARRY4
     port map (
      CI => trig_buffer_reg_i_14_n_0,
      CO(3) => trig_buffer_reg_i_9_n_0,
      CO(2) => trig_buffer_reg_i_9_n_1,
      CO(1) => trig_buffer_reg_i_9_n_2,
      CO(0) => trig_buffer_reg_i_9_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => state_n1(24 downto 21),
      S(3 downto 0) => trig_count_reg(24 downto 21)
    );
\trig_count[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => state_n(1),
      I1 => state_n(0),
      I2 => Q(1),
      O => \trig_count[0]_i_1_n_0\
    );
\trig_count[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(0),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[0]_i_3_n_0\
    );
\trig_count[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(3),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[0]_i_4_n_0\
    );
\trig_count[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(2),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[0]_i_5_n_0\
    );
\trig_count[0]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(1),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[0]_i_6_n_0\
    );
\trig_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => trig_count_reg(0),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[0]_i_7_n_0\
    );
\trig_count[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(15),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[12]_i_2_n_0\
    );
\trig_count[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(14),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[12]_i_3_n_0\
    );
\trig_count[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(13),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[12]_i_4_n_0\
    );
\trig_count[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(12),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[12]_i_5_n_0\
    );
\trig_count[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(19),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[16]_i_2_n_0\
    );
\trig_count[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(18),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[16]_i_3_n_0\
    );
\trig_count[16]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(17),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[16]_i_4_n_0\
    );
\trig_count[16]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(16),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[16]_i_5_n_0\
    );
\trig_count[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(23),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[20]_i_2_n_0\
    );
\trig_count[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(22),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[20]_i_3_n_0\
    );
\trig_count[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(21),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[20]_i_4_n_0\
    );
\trig_count[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(20),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[20]_i_5_n_0\
    );
\trig_count[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(27),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[24]_i_2_n_0\
    );
\trig_count[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(26),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[24]_i_3_n_0\
    );
\trig_count[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(25),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[24]_i_4_n_0\
    );
\trig_count[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(24),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[24]_i_5_n_0\
    );
\trig_count[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(31),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[28]_i_2_n_0\
    );
\trig_count[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(30),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[28]_i_3_n_0\
    );
\trig_count[28]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(29),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[28]_i_4_n_0\
    );
\trig_count[28]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(28),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[28]_i_5_n_0\
    );
\trig_count[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(7),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[4]_i_2_n_0\
    );
\trig_count[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(6),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[4]_i_3_n_0\
    );
\trig_count[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(5),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[4]_i_4_n_0\
    );
\trig_count[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(4),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[4]_i_5_n_0\
    );
\trig_count[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(11),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[8]_i_2_n_0\
    );
\trig_count[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(10),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[8]_i_3_n_0\
    );
\trig_count[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(9),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[8]_i_4_n_0\
    );
\trig_count[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => trig_count_reg(8),
      I1 => trig_buffer_i_2_n_0,
      O => \trig_count[8]_i_5_n_0\
    );
\trig_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[0]_i_2_n_7\,
      Q => trig_count_reg(0),
      R => '0'
    );
\trig_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_count_reg[0]_i_2_n_0\,
      CO(2) => \trig_count_reg[0]_i_2_n_1\,
      CO(1) => \trig_count_reg[0]_i_2_n_2\,
      CO(0) => \trig_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \trig_count[0]_i_3_n_0\,
      O(3) => \trig_count_reg[0]_i_2_n_4\,
      O(2) => \trig_count_reg[0]_i_2_n_5\,
      O(1) => \trig_count_reg[0]_i_2_n_6\,
      O(0) => \trig_count_reg[0]_i_2_n_7\,
      S(3) => \trig_count[0]_i_4_n_0\,
      S(2) => \trig_count[0]_i_5_n_0\,
      S(1) => \trig_count[0]_i_6_n_0\,
      S(0) => \trig_count[0]_i_7_n_0\
    );
\trig_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[8]_i_1_n_5\,
      Q => trig_count_reg(10),
      R => '0'
    );
\trig_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[8]_i_1_n_4\,
      Q => trig_count_reg(11),
      R => '0'
    );
\trig_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[12]_i_1_n_7\,
      Q => trig_count_reg(12),
      R => '0'
    );
\trig_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[8]_i_1_n_0\,
      CO(3) => \trig_count_reg[12]_i_1_n_0\,
      CO(2) => \trig_count_reg[12]_i_1_n_1\,
      CO(1) => \trig_count_reg[12]_i_1_n_2\,
      CO(0) => \trig_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[12]_i_1_n_4\,
      O(2) => \trig_count_reg[12]_i_1_n_5\,
      O(1) => \trig_count_reg[12]_i_1_n_6\,
      O(0) => \trig_count_reg[12]_i_1_n_7\,
      S(3) => \trig_count[12]_i_2_n_0\,
      S(2) => \trig_count[12]_i_3_n_0\,
      S(1) => \trig_count[12]_i_4_n_0\,
      S(0) => \trig_count[12]_i_5_n_0\
    );
\trig_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[12]_i_1_n_6\,
      Q => trig_count_reg(13),
      R => '0'
    );
\trig_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[12]_i_1_n_5\,
      Q => trig_count_reg(14),
      R => '0'
    );
\trig_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[12]_i_1_n_4\,
      Q => trig_count_reg(15),
      R => '0'
    );
\trig_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[16]_i_1_n_7\,
      Q => trig_count_reg(16),
      R => '0'
    );
\trig_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[12]_i_1_n_0\,
      CO(3) => \trig_count_reg[16]_i_1_n_0\,
      CO(2) => \trig_count_reg[16]_i_1_n_1\,
      CO(1) => \trig_count_reg[16]_i_1_n_2\,
      CO(0) => \trig_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[16]_i_1_n_4\,
      O(2) => \trig_count_reg[16]_i_1_n_5\,
      O(1) => \trig_count_reg[16]_i_1_n_6\,
      O(0) => \trig_count_reg[16]_i_1_n_7\,
      S(3) => \trig_count[16]_i_2_n_0\,
      S(2) => \trig_count[16]_i_3_n_0\,
      S(1) => \trig_count[16]_i_4_n_0\,
      S(0) => \trig_count[16]_i_5_n_0\
    );
\trig_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[16]_i_1_n_6\,
      Q => trig_count_reg(17),
      R => '0'
    );
\trig_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[16]_i_1_n_5\,
      Q => trig_count_reg(18),
      R => '0'
    );
\trig_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[16]_i_1_n_4\,
      Q => trig_count_reg(19),
      R => '0'
    );
\trig_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[0]_i_2_n_6\,
      Q => trig_count_reg(1),
      R => '0'
    );
\trig_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[20]_i_1_n_7\,
      Q => trig_count_reg(20),
      R => '0'
    );
\trig_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[16]_i_1_n_0\,
      CO(3) => \trig_count_reg[20]_i_1_n_0\,
      CO(2) => \trig_count_reg[20]_i_1_n_1\,
      CO(1) => \trig_count_reg[20]_i_1_n_2\,
      CO(0) => \trig_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[20]_i_1_n_4\,
      O(2) => \trig_count_reg[20]_i_1_n_5\,
      O(1) => \trig_count_reg[20]_i_1_n_6\,
      O(0) => \trig_count_reg[20]_i_1_n_7\,
      S(3) => \trig_count[20]_i_2_n_0\,
      S(2) => \trig_count[20]_i_3_n_0\,
      S(1) => \trig_count[20]_i_4_n_0\,
      S(0) => \trig_count[20]_i_5_n_0\
    );
\trig_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[20]_i_1_n_6\,
      Q => trig_count_reg(21),
      R => '0'
    );
\trig_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[20]_i_1_n_5\,
      Q => trig_count_reg(22),
      R => '0'
    );
\trig_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[20]_i_1_n_4\,
      Q => trig_count_reg(23),
      R => '0'
    );
\trig_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[24]_i_1_n_7\,
      Q => trig_count_reg(24),
      R => '0'
    );
\trig_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[20]_i_1_n_0\,
      CO(3) => \trig_count_reg[24]_i_1_n_0\,
      CO(2) => \trig_count_reg[24]_i_1_n_1\,
      CO(1) => \trig_count_reg[24]_i_1_n_2\,
      CO(0) => \trig_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[24]_i_1_n_4\,
      O(2) => \trig_count_reg[24]_i_1_n_5\,
      O(1) => \trig_count_reg[24]_i_1_n_6\,
      O(0) => \trig_count_reg[24]_i_1_n_7\,
      S(3) => \trig_count[24]_i_2_n_0\,
      S(2) => \trig_count[24]_i_3_n_0\,
      S(1) => \trig_count[24]_i_4_n_0\,
      S(0) => \trig_count[24]_i_5_n_0\
    );
\trig_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[24]_i_1_n_6\,
      Q => trig_count_reg(25),
      R => '0'
    );
\trig_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[24]_i_1_n_5\,
      Q => trig_count_reg(26),
      R => '0'
    );
\trig_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[24]_i_1_n_4\,
      Q => trig_count_reg(27),
      R => '0'
    );
\trig_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[28]_i_1_n_7\,
      Q => trig_count_reg(28),
      R => '0'
    );
\trig_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_trig_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \trig_count_reg[28]_i_1_n_1\,
      CO(1) => \trig_count_reg[28]_i_1_n_2\,
      CO(0) => \trig_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[28]_i_1_n_4\,
      O(2) => \trig_count_reg[28]_i_1_n_5\,
      O(1) => \trig_count_reg[28]_i_1_n_6\,
      O(0) => \trig_count_reg[28]_i_1_n_7\,
      S(3) => \trig_count[28]_i_2_n_0\,
      S(2) => \trig_count[28]_i_3_n_0\,
      S(1) => \trig_count[28]_i_4_n_0\,
      S(0) => \trig_count[28]_i_5_n_0\
    );
\trig_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[28]_i_1_n_6\,
      Q => trig_count_reg(29),
      R => '0'
    );
\trig_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[0]_i_2_n_5\,
      Q => trig_count_reg(2),
      R => '0'
    );
\trig_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[28]_i_1_n_5\,
      Q => trig_count_reg(30),
      R => '0'
    );
\trig_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[28]_i_1_n_4\,
      Q => trig_count_reg(31),
      R => '0'
    );
\trig_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[0]_i_2_n_4\,
      Q => trig_count_reg(3),
      R => '0'
    );
\trig_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[4]_i_1_n_7\,
      Q => trig_count_reg(4),
      R => '0'
    );
\trig_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[0]_i_2_n_0\,
      CO(3) => \trig_count_reg[4]_i_1_n_0\,
      CO(2) => \trig_count_reg[4]_i_1_n_1\,
      CO(1) => \trig_count_reg[4]_i_1_n_2\,
      CO(0) => \trig_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[4]_i_1_n_4\,
      O(2) => \trig_count_reg[4]_i_1_n_5\,
      O(1) => \trig_count_reg[4]_i_1_n_6\,
      O(0) => \trig_count_reg[4]_i_1_n_7\,
      S(3) => \trig_count[4]_i_2_n_0\,
      S(2) => \trig_count[4]_i_3_n_0\,
      S(1) => \trig_count[4]_i_4_n_0\,
      S(0) => \trig_count[4]_i_5_n_0\
    );
\trig_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[4]_i_1_n_6\,
      Q => trig_count_reg(5),
      R => '0'
    );
\trig_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[4]_i_1_n_5\,
      Q => trig_count_reg(6),
      R => '0'
    );
\trig_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[4]_i_1_n_4\,
      Q => trig_count_reg(7),
      R => '0'
    );
\trig_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[8]_i_1_n_7\,
      Q => trig_count_reg(8),
      R => '0'
    );
\trig_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_count_reg[4]_i_1_n_0\,
      CO(3) => \trig_count_reg[8]_i_1_n_0\,
      CO(2) => \trig_count_reg[8]_i_1_n_1\,
      CO(1) => \trig_count_reg[8]_i_1_n_2\,
      CO(0) => \trig_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_count_reg[8]_i_1_n_4\,
      O(2) => \trig_count_reg[8]_i_1_n_5\,
      O(1) => \trig_count_reg[8]_i_1_n_6\,
      O(0) => \trig_count_reg[8]_i_1_n_7\,
      S(3) => \trig_count[8]_i_2_n_0\,
      S(2) => \trig_count[8]_i_3_n_0\,
      S(1) => \trig_count[8]_i_4_n_0\,
      S(0) => \trig_count[8]_i_5_n_0\
    );
\trig_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \trig_count[0]_i_1_n_0\,
      D => \trig_count_reg[8]_i_1_n_6\,
      Q => trig_count_reg(9),
      R => '0'
    );
trig_reg: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => Q(1),
      D => trig_buffer,
      Q => trig
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0_S00_AXI is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    trig : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    echo : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rst : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair14";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s00_axi_awvalid,
      I2 => s00_axi_wvalid,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => axi_awready_i_1_n_0
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      S => axi_awready_i_1_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      S => axi_awready_i_1_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => axi_awready_i_1_n_0
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s_axi_awready\,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => axi_awready_i_1_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => axi_awready_i_1_n_0
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CAF0CA00"
    )
        port map (
      I0 => slv_reg1(26),
      I1 => slv_reg3(26),
      I2 => axi_araddr(3),
      I3 => axi_araddr(2),
      I4 => slv_reg2(26),
      O => reg_data_out(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CAF0CA00"
    )
        port map (
      I0 => slv_reg1(27),
      I1 => slv_reg3(27),
      I2 => axi_araddr(3),
      I3 => axi_araddr(2),
      I4 => slv_reg2(27),
      O => reg_data_out(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CAF0CA00"
    )
        port map (
      I0 => slv_reg1(28),
      I1 => slv_reg3(28),
      I2 => axi_araddr(3),
      I3 => axi_araddr(2),
      I4 => slv_reg2(28),
      O => reg_data_out(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CAF0CA00"
    )
        port map (
      I0 => slv_reg1(29),
      I1 => slv_reg3(29),
      I2 => axi_araddr(3),
      I3 => axi_araddr(2),
      I4 => slv_reg2(29),
      O => reg_data_out(29)
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CAF0CA00"
    )
        port map (
      I0 => slv_reg1(30),
      I1 => slv_reg3(30),
      I2 => axi_araddr(3),
      I3 => axi_araddr(2),
      I4 => slv_reg2(30),
      O => reg_data_out(30)
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CAF0CA00"
    )
        port map (
      I0 => slv_reg1(31),
      I1 => slv_reg3(31),
      I2 => axi_araddr(3),
      I3 => axi_araddr(2),
      I4 => slv_reg2(31),
      O => reg_data_out(31)
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => axi_awready_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => axi_awready_i_1_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => axi_awready_i_1_n_0
    );
hcsr_driver: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srhc_fsm
     port map (
      D(25 downto 0) => reg_data_out(25 downto 0),
      Q(25 downto 2) => slv_reg1(25 downto 2),
      Q(1) => rst,
      Q(0) => slv_reg1(0),
      axi_araddr(1 downto 0) => axi_araddr(3 downto 2),
      \axi_rdata_reg[25]\(25 downto 0) => slv_reg3(25 downto 0),
      \axi_rdata_reg[25]_0\(25 downto 0) => slv_reg2(25 downto 0),
      clk => clk,
      echo => echo,
      trig => trig
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => rst,
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(0),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg2(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg2(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg2(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg2(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg2(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg2(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg2(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg2(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg2(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg2(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg2(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg2(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg2(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg2(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg2(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg2(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg2(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg2(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg2(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg2(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg2(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg2(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg2(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg2(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg2(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg2(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg2(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg2(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg2(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg2(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg2(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg2(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => p_1_in(15)
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => p_1_in(23)
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => p_1_in(31)
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => p_1_in(7)
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg3(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg3(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg3(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg3(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg3(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg3(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg3(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg3(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg3(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg3(9),
      R => axi_awready_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0 is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    trig : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    echo : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0 is
begin
HCSR04_Final_v1_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      clk => clk,
      echo => echo,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(1 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(1 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      trig => trig
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    trig : out STD_LOGIC;
    echo : in STD_LOGIC;
    clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_HCSR04_Final_0_0,HCSR04_Final_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "HCSR04_Final_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HCSR04_Final_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      clk => clk,
      echo => echo,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(3 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(3 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      trig => trig
    );
end STRUCTURE;

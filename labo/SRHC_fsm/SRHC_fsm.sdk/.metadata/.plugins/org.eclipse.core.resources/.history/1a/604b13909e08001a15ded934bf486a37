/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

//#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "HCSR04_Final.h"
#include "sleep.h"

#define ULTRASONIC_ADDR XPAR_HCSR04_FINAL_0_S00_AXI_BASEADDR
#define ULTRASONIC_REG0 HCSR04_FINAL_S00_AXI_SLV_REG0_OFFSET
#define ULTRASONIC_REG1 HCSR04_FINAL_S00_AXI_SLV_REG1_OFFSET

#define CHECKBIT(var, pos)  ((var) & (1<<(pos)))
#define CHECKRDY(var) (CHECKBIT(var, 31))
#define CHECKOOB(var) (CHECKBIT(var, 30))

int main()
{
    init_platform();
    xil_printf("Ultrasonic test.\n\r");
    u32 ultrasonic_data = 0u;

    while(1){
    	ultrasonic_data = 0u;
    	xil_printf("Reading: \r\n");
    	Xil_Out32(ULTRASONIC_ADDR+ULTRASONIC_REG1, 0x00000001);

    	usleep_A9(500000);
    	xil_printf("Data: 0x%08x\r\n", Xil_In32(ULTRASONIC_ADDR+ULTRASONIC_REG0));

    	while(CHECKRDY(Xil_In32(ULTRASONIC_ADDR+ULTRASONIC_REG0))){
    		xil_printf("Waiting\r\n");
    	}
    	xil_printf("Data: 0x%08x\r\n", Xil_In32(ULTRASONIC_ADDR+ULTRASONIC_REG0));
    	ultrasonic_data = Xil_In32(ULTRASONIC_ADDR+ULTRASONIC_REG0);
    	if(CHECKOOB(Xil_In32(ULTRASONIC_ADDR+ULTRASONIC_REG0))) {
    		xil_printf("Object is too far away.\r\n");
    	}

    	xil_printf("Raw data: 0x%08x\r\n", ultrasonic_data);
    	u32 distance_data = ultrasonic_data & 0x00FFFFFF;
    	u32 dist_cm = (distance_data / 50) / 58;

    	xil_printf("Distance in cm: %d\r\n", dist_cm);

    	Xil_Out32(ULTRASONIC_ADDR+ULTRASONIC_REG1, 0x00000000);
    	usleep_A9(500000);
    }
    cleanup_platform();
    return 0;
}

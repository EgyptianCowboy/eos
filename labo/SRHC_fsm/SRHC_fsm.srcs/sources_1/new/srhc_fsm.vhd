----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/15/2019 02:38:52 PM
-- Design Name: 
-- Module Name: srhc_fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity srhc_fsm is
    generic (trig_wait : integer := 500;
             dist_max  : unsigned(23 downto 0) := x"1E8480");
    Port (trig, oob, ready      : out std_logic;
          dist_out              : out std_logic_vector(23 downto 0);
          echo, clk, rst, start : in  std_logic);
end srhc_fsm;

architecture Behavioral of srhc_fsm is
    -- signal dist_out_buffer : std_logic_vector(23 downto 0) := (others => '0');
    -- signal oob_buffer      : std_logic := '0';

    type state_t is (wait_start, send_trig, wait_re_echo, wait_fe_echo);
begin
-- purpose: Process to measure distance with a HC-SR04
-- type   : sequential
-- inputs : clk, rst, echo
-- outputs: trig, oob, dist_out
    measurement : process (clk, rst) is
        variable resync_echo, resync_start  : std_logic_vector(1 to 3);
        variable fe_echo, re_echo, re_start : std_logic;
        variable state_n, state_p           : state_t;
        variable trig_count : integer := 0;
        variable dist_out_buffer : std_logic_vector(23 downto 0) := (others => '0');
        variable trig_buffer, oob_buffer, ready_buffer : std_logic;
    begin  -- process measurement
        if rst = '1' then                   -- asynchronous reset (active high)
            dist_out_buffer := (others => '0');
            oob_buffer      := '0';
            trig_buffer     := '0';
            ready_buffer    := '0';
            trig <= trig_buffer;
            oob <= oob_buffer;
            ready <= ready_buffer;
            state_n         := wait_start;
            state_n         := wait_start;
        elsif clk'event and clk = '1' then  -- rising clock edge
            resync_echo  := echo & resync_echo(1 to 2);
            resync_start := start & resync_start(1 to 2);

            re_echo  := (resync_echo(2) and not resync_echo(3));
            fe_echo  := (resync_echo(3) and not resync_echo(2));
            re_start := (resync_start(2) and not resync_start(3));

            trig <= trig_buffer;
            oob <= oob_buffer;
            ready <= ready_buffer;

            state_p := state_n;
            case state_p is
                when wait_start =>
                    dist_out <= dist_out_buffer;
                    ready_buffer := '1';
                    if re_start = '1' then
                        dist_out_buffer := (others => '0');
                        ready_buffer := '0';
                        oob_buffer := '0';
                        state_n := send_trig;
                        trig_buffer := '1';
                    end if;
                when send_trig =>
                    trig_count := trig_count + 1;
                    if trig_count = 500 then
                        trig_count := 0;
                        trig_buffer := '0';
                        state_n := wait_re_echo;
                    end if;
                when wait_re_echo =>
                    if re_echo = '1' then
                        state_n := wait_fe_echo;
                    end if;
                when wait_fe_echo =>
                    dist_out_buffer := std_logic_vector(unsigned(dist_out_buffer) + 1);
                    if fe_echo = '1' then
                        state_n := wait_start;
                    elsif unsigned(dist_out_buffer) = dist_max then
                        oob_buffer := '1';
                        state_n := wait_start;
                    end if;
                when others => null;
            end case;
        end if;
    end process measurement;
end Behavioral;

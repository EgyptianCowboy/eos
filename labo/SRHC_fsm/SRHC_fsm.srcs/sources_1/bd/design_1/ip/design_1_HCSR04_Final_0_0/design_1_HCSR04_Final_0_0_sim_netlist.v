// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Nov 16 18:42:54 2019
// Host        : t480s running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /home/sil/Documents/school/EOS/labo/SRHC_fsm/SRHC_fsm.srcs/sources_1/bd/design_1/ip/design_1_HCSR04_Final_0_0/design_1_HCSR04_Final_0_0_sim_netlist.v
// Design      : design_1_HCSR04_Final_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_HCSR04_Final_0_0,HCSR04_Final_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "HCSR04_Final_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_HCSR04_Final_0_0
   (trig,
    echo,
    clk,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  output trig;
  input echo;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF clk, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input clk;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire clk;
  wire echo;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire trig;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_HCSR04_Final_0_0_HCSR04_Final_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .clk(clk),
        .echo(echo),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .trig(trig));
endmodule

(* ORIG_REF_NAME = "HCSR04_Final_v1_0" *) 
module design_1_HCSR04_Final_0_0_HCSR04_Final_v1_0
   (S_AXI_AWREADY,
    trig,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    clk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    echo,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output trig;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input clk;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input echo;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire clk;
  wire echo;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire trig;

  design_1_HCSR04_Final_0_0_HCSR04_Final_v1_0_S00_AXI HCSR04_Final_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .clk(clk),
        .echo(echo),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .trig(trig));
endmodule

(* ORIG_REF_NAME = "HCSR04_Final_v1_0_S00_AXI" *) 
module design_1_HCSR04_Final_0_0_HCSR04_Final_v1_0_S00_AXI
   (S_AXI_AWREADY,
    trig,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    clk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    echo,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output trig;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input clk;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input echo;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire clk;
  wire echo;
  wire [1:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire rst;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire trig;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg1[24]),
        .I1(slv_reg3[24]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[24]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg1[25]),
        .I1(slv_reg3[25]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[25]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg1[26]),
        .I1(slv_reg3[26]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[26]),
        .O(reg_data_out[26]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg1[27]),
        .I1(slv_reg3[27]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[27]),
        .O(reg_data_out[27]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg1[28]),
        .I1(slv_reg3[28]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[28]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg1[29]),
        .I1(slv_reg3[29]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[29]),
        .O(reg_data_out[29]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  design_1_HCSR04_Final_0_0_srhc_fsm hcsr_driver
       (.D({reg_data_out[31:30],reg_data_out[23:0]}),
        .Q({slv_reg1[31:30],slv_reg1[23:2],rst,slv_reg1[0]}),
        .axi_araddr(axi_araddr),
        .\axi_rdata_reg[31] ({slv_reg3[31:30],slv_reg3[23:0]}),
        .\axi_rdata_reg[31]_0 ({slv_reg2[31:30],slv_reg2[23:0]}),
        .clk(clk),
        .echo(echo),
        .trig(trig));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg1[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(rst),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
endmodule

(* ORIG_REF_NAME = "srhc_fsm" *) 
module design_1_HCSR04_Final_0_0_srhc_fsm
   (trig,
    D,
    clk,
    Q,
    \axi_rdata_reg[31] ,
    axi_araddr,
    \axi_rdata_reg[31]_0 ,
    echo);
  output trig;
  output [25:0]D;
  input clk;
  input [25:0]Q;
  input [25:0]\axi_rdata_reg[31] ;
  input [1:0]axi_araddr;
  input [25:0]\axi_rdata_reg[31]_0 ;
  input echo;

  wire [25:0]D;
  wire \FSM_sequential_state_n[0]_i_1_n_0 ;
  wire \FSM_sequential_state_n[1]_i_1_n_0 ;
  wire \FSM_sequential_state_n[1]_i_2_n_0 ;
  wire \FSM_sequential_state_n[1]_i_4_n_0 ;
  wire \FSM_sequential_state_n[1]_i_5_n_0 ;
  wire \FSM_sequential_state_n[1]_i_6_n_0 ;
  wire \FSM_sequential_state_n[1]_i_7_n_0 ;
  wire [25:0]Q;
  wire [1:0]axi_araddr;
  wire [25:0]\axi_rdata_reg[31] ;
  wire [25:0]\axi_rdata_reg[31]_0 ;
  wire clk;
  wire [23:0]dist_out;
  wire \dist_out[23]_i_1_n_0 ;
  wire [0:0]dist_out_buffer;
  wire \dist_out_buffer[10]_i_1_n_0 ;
  wire \dist_out_buffer[11]_i_1_n_0 ;
  wire \dist_out_buffer[12]_i_1_n_0 ;
  wire \dist_out_buffer[13]_i_1_n_0 ;
  wire \dist_out_buffer[14]_i_1_n_0 ;
  wire \dist_out_buffer[15]_i_1_n_0 ;
  wire \dist_out_buffer[16]_i_1_n_0 ;
  wire \dist_out_buffer[17]_i_1_n_0 ;
  wire \dist_out_buffer[18]_i_1_n_0 ;
  wire \dist_out_buffer[19]_i_1_n_0 ;
  wire \dist_out_buffer[1]_i_1_n_0 ;
  wire \dist_out_buffer[20]_i_1_n_0 ;
  wire \dist_out_buffer[21]_i_1_n_0 ;
  wire \dist_out_buffer[22]_i_1_n_0 ;
  wire \dist_out_buffer[23]_i_1_n_0 ;
  wire \dist_out_buffer[23]_i_2_n_0 ;
  wire \dist_out_buffer[2]_i_1_n_0 ;
  wire \dist_out_buffer[3]_i_1_n_0 ;
  wire \dist_out_buffer[4]_i_1_n_0 ;
  wire \dist_out_buffer[5]_i_1_n_0 ;
  wire \dist_out_buffer[6]_i_1_n_0 ;
  wire \dist_out_buffer[7]_i_1_n_0 ;
  wire \dist_out_buffer[8]_i_1_n_0 ;
  wire \dist_out_buffer[9]_i_1_n_0 ;
  wire \dist_out_buffer_reg[12]_i_2_n_0 ;
  wire \dist_out_buffer_reg[12]_i_2_n_1 ;
  wire \dist_out_buffer_reg[12]_i_2_n_2 ;
  wire \dist_out_buffer_reg[12]_i_2_n_3 ;
  wire \dist_out_buffer_reg[16]_i_2_n_0 ;
  wire \dist_out_buffer_reg[16]_i_2_n_1 ;
  wire \dist_out_buffer_reg[16]_i_2_n_2 ;
  wire \dist_out_buffer_reg[16]_i_2_n_3 ;
  wire \dist_out_buffer_reg[20]_i_2_n_0 ;
  wire \dist_out_buffer_reg[20]_i_2_n_1 ;
  wire \dist_out_buffer_reg[20]_i_2_n_2 ;
  wire \dist_out_buffer_reg[20]_i_2_n_3 ;
  wire \dist_out_buffer_reg[23]_i_3_n_2 ;
  wire \dist_out_buffer_reg[23]_i_3_n_3 ;
  wire \dist_out_buffer_reg[4]_i_2_n_0 ;
  wire \dist_out_buffer_reg[4]_i_2_n_1 ;
  wire \dist_out_buffer_reg[4]_i_2_n_2 ;
  wire \dist_out_buffer_reg[4]_i_2_n_3 ;
  wire \dist_out_buffer_reg[8]_i_2_n_0 ;
  wire \dist_out_buffer_reg[8]_i_2_n_1 ;
  wire \dist_out_buffer_reg[8]_i_2_n_2 ;
  wire \dist_out_buffer_reg[8]_i_2_n_3 ;
  wire \dist_out_buffer_reg_n_0_[0] ;
  wire \dist_out_buffer_reg_n_0_[10] ;
  wire \dist_out_buffer_reg_n_0_[11] ;
  wire \dist_out_buffer_reg_n_0_[12] ;
  wire \dist_out_buffer_reg_n_0_[13] ;
  wire \dist_out_buffer_reg_n_0_[14] ;
  wire \dist_out_buffer_reg_n_0_[15] ;
  wire \dist_out_buffer_reg_n_0_[16] ;
  wire \dist_out_buffer_reg_n_0_[17] ;
  wire \dist_out_buffer_reg_n_0_[18] ;
  wire \dist_out_buffer_reg_n_0_[19] ;
  wire \dist_out_buffer_reg_n_0_[1] ;
  wire \dist_out_buffer_reg_n_0_[20] ;
  wire \dist_out_buffer_reg_n_0_[21] ;
  wire \dist_out_buffer_reg_n_0_[22] ;
  wire \dist_out_buffer_reg_n_0_[23] ;
  wire \dist_out_buffer_reg_n_0_[2] ;
  wire \dist_out_buffer_reg_n_0_[3] ;
  wire \dist_out_buffer_reg_n_0_[4] ;
  wire \dist_out_buffer_reg_n_0_[5] ;
  wire \dist_out_buffer_reg_n_0_[6] ;
  wire \dist_out_buffer_reg_n_0_[7] ;
  wire \dist_out_buffer_reg_n_0_[8] ;
  wire \dist_out_buffer_reg_n_0_[9] ;
  wire echo;
  wire [23:1]in3;
  wire oob;
  wire oob_buffer;
  wire oob_buffer0__15;
  wire oob_buffer1__0;
  wire oob_buffer_i_1_n_0;
  wire p_1_in;
  wire p_1_in3_in;
  wire ready;
  wire ready_buffer;
  wire ready_buffer1__0;
  wire ready_buffer_i_1_n_0;
  wire \resync_echo_reg_n_0_[2] ;
  wire \resync_start[1]_i_1_n_0 ;
  wire \resync_start_reg_n_0_[2] ;
  wire [1:0]state_n;
  wire [31:1]state_n1;
  wire trig;
  wire trig_buffer;
  wire trig_buffer_i_1_n_0;
  wire trig_buffer_i_2_n_0;
  wire trig_buffer_i_3_n_0;
  wire trig_buffer_i_4_n_0;
  wire trig_buffer_i_5_n_0;
  wire trig_buffer_i_6_n_0;
  wire trig_buffer_i_7_n_0;
  wire trig_buffer_i_8_n_0;
  wire trig_buffer_reg_i_10_n_0;
  wire trig_buffer_reg_i_10_n_1;
  wire trig_buffer_reg_i_10_n_2;
  wire trig_buffer_reg_i_10_n_3;
  wire trig_buffer_reg_i_11_n_2;
  wire trig_buffer_reg_i_11_n_3;
  wire trig_buffer_reg_i_12_n_0;
  wire trig_buffer_reg_i_12_n_1;
  wire trig_buffer_reg_i_12_n_2;
  wire trig_buffer_reg_i_12_n_3;
  wire trig_buffer_reg_i_13_n_0;
  wire trig_buffer_reg_i_13_n_1;
  wire trig_buffer_reg_i_13_n_2;
  wire trig_buffer_reg_i_13_n_3;
  wire trig_buffer_reg_i_14_n_0;
  wire trig_buffer_reg_i_14_n_1;
  wire trig_buffer_reg_i_14_n_2;
  wire trig_buffer_reg_i_14_n_3;
  wire trig_buffer_reg_i_15_n_0;
  wire trig_buffer_reg_i_15_n_1;
  wire trig_buffer_reg_i_15_n_2;
  wire trig_buffer_reg_i_15_n_3;
  wire trig_buffer_reg_i_16_n_0;
  wire trig_buffer_reg_i_16_n_1;
  wire trig_buffer_reg_i_16_n_2;
  wire trig_buffer_reg_i_16_n_3;
  wire trig_buffer_reg_i_9_n_0;
  wire trig_buffer_reg_i_9_n_1;
  wire trig_buffer_reg_i_9_n_2;
  wire trig_buffer_reg_i_9_n_3;
  wire \trig_count[0]_i_1_n_0 ;
  wire \trig_count[0]_i_3_n_0 ;
  wire \trig_count[0]_i_4_n_0 ;
  wire \trig_count[0]_i_5_n_0 ;
  wire \trig_count[0]_i_6_n_0 ;
  wire \trig_count[0]_i_7_n_0 ;
  wire \trig_count[12]_i_2_n_0 ;
  wire \trig_count[12]_i_3_n_0 ;
  wire \trig_count[12]_i_4_n_0 ;
  wire \trig_count[12]_i_5_n_0 ;
  wire \trig_count[16]_i_2_n_0 ;
  wire \trig_count[16]_i_3_n_0 ;
  wire \trig_count[16]_i_4_n_0 ;
  wire \trig_count[16]_i_5_n_0 ;
  wire \trig_count[20]_i_2_n_0 ;
  wire \trig_count[20]_i_3_n_0 ;
  wire \trig_count[20]_i_4_n_0 ;
  wire \trig_count[20]_i_5_n_0 ;
  wire \trig_count[24]_i_2_n_0 ;
  wire \trig_count[24]_i_3_n_0 ;
  wire \trig_count[24]_i_4_n_0 ;
  wire \trig_count[24]_i_5_n_0 ;
  wire \trig_count[28]_i_2_n_0 ;
  wire \trig_count[28]_i_3_n_0 ;
  wire \trig_count[28]_i_4_n_0 ;
  wire \trig_count[28]_i_5_n_0 ;
  wire \trig_count[4]_i_2_n_0 ;
  wire \trig_count[4]_i_3_n_0 ;
  wire \trig_count[4]_i_4_n_0 ;
  wire \trig_count[4]_i_5_n_0 ;
  wire \trig_count[8]_i_2_n_0 ;
  wire \trig_count[8]_i_3_n_0 ;
  wire \trig_count[8]_i_4_n_0 ;
  wire \trig_count[8]_i_5_n_0 ;
  wire [31:0]trig_count_reg;
  wire \trig_count_reg[0]_i_2_n_0 ;
  wire \trig_count_reg[0]_i_2_n_1 ;
  wire \trig_count_reg[0]_i_2_n_2 ;
  wire \trig_count_reg[0]_i_2_n_3 ;
  wire \trig_count_reg[0]_i_2_n_4 ;
  wire \trig_count_reg[0]_i_2_n_5 ;
  wire \trig_count_reg[0]_i_2_n_6 ;
  wire \trig_count_reg[0]_i_2_n_7 ;
  wire \trig_count_reg[12]_i_1_n_0 ;
  wire \trig_count_reg[12]_i_1_n_1 ;
  wire \trig_count_reg[12]_i_1_n_2 ;
  wire \trig_count_reg[12]_i_1_n_3 ;
  wire \trig_count_reg[12]_i_1_n_4 ;
  wire \trig_count_reg[12]_i_1_n_5 ;
  wire \trig_count_reg[12]_i_1_n_6 ;
  wire \trig_count_reg[12]_i_1_n_7 ;
  wire \trig_count_reg[16]_i_1_n_0 ;
  wire \trig_count_reg[16]_i_1_n_1 ;
  wire \trig_count_reg[16]_i_1_n_2 ;
  wire \trig_count_reg[16]_i_1_n_3 ;
  wire \trig_count_reg[16]_i_1_n_4 ;
  wire \trig_count_reg[16]_i_1_n_5 ;
  wire \trig_count_reg[16]_i_1_n_6 ;
  wire \trig_count_reg[16]_i_1_n_7 ;
  wire \trig_count_reg[20]_i_1_n_0 ;
  wire \trig_count_reg[20]_i_1_n_1 ;
  wire \trig_count_reg[20]_i_1_n_2 ;
  wire \trig_count_reg[20]_i_1_n_3 ;
  wire \trig_count_reg[20]_i_1_n_4 ;
  wire \trig_count_reg[20]_i_1_n_5 ;
  wire \trig_count_reg[20]_i_1_n_6 ;
  wire \trig_count_reg[20]_i_1_n_7 ;
  wire \trig_count_reg[24]_i_1_n_0 ;
  wire \trig_count_reg[24]_i_1_n_1 ;
  wire \trig_count_reg[24]_i_1_n_2 ;
  wire \trig_count_reg[24]_i_1_n_3 ;
  wire \trig_count_reg[24]_i_1_n_4 ;
  wire \trig_count_reg[24]_i_1_n_5 ;
  wire \trig_count_reg[24]_i_1_n_6 ;
  wire \trig_count_reg[24]_i_1_n_7 ;
  wire \trig_count_reg[28]_i_1_n_1 ;
  wire \trig_count_reg[28]_i_1_n_2 ;
  wire \trig_count_reg[28]_i_1_n_3 ;
  wire \trig_count_reg[28]_i_1_n_4 ;
  wire \trig_count_reg[28]_i_1_n_5 ;
  wire \trig_count_reg[28]_i_1_n_6 ;
  wire \trig_count_reg[28]_i_1_n_7 ;
  wire \trig_count_reg[4]_i_1_n_0 ;
  wire \trig_count_reg[4]_i_1_n_1 ;
  wire \trig_count_reg[4]_i_1_n_2 ;
  wire \trig_count_reg[4]_i_1_n_3 ;
  wire \trig_count_reg[4]_i_1_n_4 ;
  wire \trig_count_reg[4]_i_1_n_5 ;
  wire \trig_count_reg[4]_i_1_n_6 ;
  wire \trig_count_reg[4]_i_1_n_7 ;
  wire \trig_count_reg[8]_i_1_n_0 ;
  wire \trig_count_reg[8]_i_1_n_1 ;
  wire \trig_count_reg[8]_i_1_n_2 ;
  wire \trig_count_reg[8]_i_1_n_3 ;
  wire \trig_count_reg[8]_i_1_n_4 ;
  wire \trig_count_reg[8]_i_1_n_5 ;
  wire \trig_count_reg[8]_i_1_n_6 ;
  wire \trig_count_reg[8]_i_1_n_7 ;
  wire [3:2]\NLW_dist_out_buffer_reg[23]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_dist_out_buffer_reg[23]_i_3_O_UNCONNECTED ;
  wire [3:2]NLW_trig_buffer_reg_i_11_CO_UNCONNECTED;
  wire [3:3]NLW_trig_buffer_reg_i_11_O_UNCONNECTED;
  wire [3:3]\NLW_trig_count_reg[28]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h110155550F00AAAA)) 
    \FSM_sequential_state_n[0]_i_1 
       (.I0(\FSM_sequential_state_n[1]_i_2_n_0 ),
        .I1(oob_buffer0__15),
        .I2(\resync_echo_reg_n_0_[2] ),
        .I3(p_1_in),
        .I4(state_n[1]),
        .I5(state_n[0]),
        .O(\FSM_sequential_state_n[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1101AAAA5F550000)) 
    \FSM_sequential_state_n[1]_i_1 
       (.I0(\FSM_sequential_state_n[1]_i_2_n_0 ),
        .I1(oob_buffer0__15),
        .I2(\resync_echo_reg_n_0_[2] ),
        .I3(p_1_in),
        .I4(state_n[1]),
        .I5(state_n[0]),
        .O(\FSM_sequential_state_n[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00F20002)) 
    \FSM_sequential_state_n[1]_i_2 
       (.I0(p_1_in3_in),
        .I1(\resync_start_reg_n_0_[2] ),
        .I2(state_n[0]),
        .I3(state_n[1]),
        .I4(trig_buffer_i_2_n_0),
        .O(\FSM_sequential_state_n[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEEEEEEFE)) 
    \FSM_sequential_state_n[1]_i_3 
       (.I0(in3[22]),
        .I1(in3[21]),
        .I2(in3[17]),
        .I3(\FSM_sequential_state_n[1]_i_4_n_0 ),
        .I4(\FSM_sequential_state_n[1]_i_5_n_0 ),
        .I5(in3[23]),
        .O(oob_buffer0__15));
  LUT6 #(
    .INIT(64'h555555557777777F)) 
    \FSM_sequential_state_n[1]_i_4 
       (.I0(in3[20]),
        .I1(in3[15]),
        .I2(\FSM_sequential_state_n[1]_i_6_n_0 ),
        .I3(\FSM_sequential_state_n[1]_i_7_n_0 ),
        .I4(in3[11]),
        .I5(in3[16]),
        .O(\FSM_sequential_state_n[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_sequential_state_n[1]_i_5 
       (.I0(in3[18]),
        .I1(in3[19]),
        .O(\FSM_sequential_state_n[1]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_state_n[1]_i_6 
       (.I0(in3[12]),
        .I1(in3[13]),
        .O(\FSM_sequential_state_n[1]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFEAAAA)) 
    \FSM_sequential_state_n[1]_i_7 
       (.I0(in3[14]),
        .I1(in3[8]),
        .I2(in3[7]),
        .I3(in3[9]),
        .I4(in3[10]),
        .O(\FSM_sequential_state_n[1]_i_7_n_0 ));
  (* FSM_ENCODED_STATES = "send_trig:01,wait_re_echo:10,wait_start:00,wait_fe_echo:11" *) 
  FDCE \FSM_sequential_state_n_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(\FSM_sequential_state_n[0]_i_1_n_0 ),
        .Q(state_n[0]));
  (* FSM_ENCODED_STATES = "send_trig:01,wait_re_echo:10,wait_start:00,wait_fe_echo:11" *) 
  FDCE \FSM_sequential_state_n_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(\FSM_sequential_state_n[1]_i_1_n_0 ),
        .Q(state_n[1]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[0]_i_1 
       (.I0(Q[0]),
        .I1(dist_out[0]),
        .I2(\axi_rdata_reg[31] [0]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[10]_i_1 
       (.I0(Q[10]),
        .I1(dist_out[10]),
        .I2(\axi_rdata_reg[31] [10]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [10]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[11]_i_1 
       (.I0(Q[11]),
        .I1(dist_out[11]),
        .I2(\axi_rdata_reg[31] [11]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [11]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[12]_i_1 
       (.I0(Q[12]),
        .I1(dist_out[12]),
        .I2(\axi_rdata_reg[31] [12]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [12]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[13]_i_1 
       (.I0(Q[13]),
        .I1(dist_out[13]),
        .I2(\axi_rdata_reg[31] [13]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [13]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[14]_i_1 
       (.I0(Q[14]),
        .I1(dist_out[14]),
        .I2(\axi_rdata_reg[31] [14]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [14]),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[15]_i_1 
       (.I0(Q[15]),
        .I1(dist_out[15]),
        .I2(\axi_rdata_reg[31] [15]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [15]),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[16]_i_1 
       (.I0(Q[16]),
        .I1(dist_out[16]),
        .I2(\axi_rdata_reg[31] [16]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [16]),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[17]_i_1 
       (.I0(Q[17]),
        .I1(dist_out[17]),
        .I2(\axi_rdata_reg[31] [17]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [17]),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[18]_i_1 
       (.I0(Q[18]),
        .I1(dist_out[18]),
        .I2(\axi_rdata_reg[31] [18]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [18]),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[19]_i_1 
       (.I0(Q[19]),
        .I1(dist_out[19]),
        .I2(\axi_rdata_reg[31] [19]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [19]),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[1]_i_1 
       (.I0(Q[1]),
        .I1(dist_out[1]),
        .I2(\axi_rdata_reg[31] [1]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[20]_i_1 
       (.I0(Q[20]),
        .I1(dist_out[20]),
        .I2(\axi_rdata_reg[31] [20]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [20]),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[21]_i_1 
       (.I0(Q[21]),
        .I1(dist_out[21]),
        .I2(\axi_rdata_reg[31] [21]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [21]),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[22]_i_1 
       (.I0(Q[22]),
        .I1(dist_out[22]),
        .I2(\axi_rdata_reg[31] [22]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [22]),
        .O(D[22]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[23]_i_1 
       (.I0(Q[23]),
        .I1(dist_out[23]),
        .I2(\axi_rdata_reg[31] [23]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [23]),
        .O(D[23]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[2]_i_1 
       (.I0(Q[2]),
        .I1(dist_out[2]),
        .I2(\axi_rdata_reg[31] [2]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[30]_i_1 
       (.I0(Q[24]),
        .I1(oob),
        .I2(\axi_rdata_reg[31] [24]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [24]),
        .O(D[24]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[31]_i_2 
       (.I0(Q[25]),
        .I1(ready),
        .I2(\axi_rdata_reg[31] [25]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [25]),
        .O(D[25]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[3]_i_1 
       (.I0(Q[3]),
        .I1(dist_out[3]),
        .I2(\axi_rdata_reg[31] [3]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[4]_i_1 
       (.I0(Q[4]),
        .I1(dist_out[4]),
        .I2(\axi_rdata_reg[31] [4]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [4]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[5]_i_1 
       (.I0(Q[5]),
        .I1(dist_out[5]),
        .I2(\axi_rdata_reg[31] [5]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [5]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[6]_i_1 
       (.I0(Q[6]),
        .I1(dist_out[6]),
        .I2(\axi_rdata_reg[31] [6]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [6]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[7]_i_1 
       (.I0(Q[7]),
        .I1(dist_out[7]),
        .I2(\axi_rdata_reg[31] [7]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [7]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[8]_i_1 
       (.I0(Q[8]),
        .I1(dist_out[8]),
        .I2(\axi_rdata_reg[31] [8]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [8]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[9]_i_1 
       (.I0(Q[9]),
        .I1(dist_out[9]),
        .I2(\axi_rdata_reg[31] [9]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31]_0 [9]),
        .O(D[9]));
  LUT3 #(
    .INIT(8'h01)) 
    \dist_out[23]_i_1 
       (.I0(state_n[1]),
        .I1(state_n[0]),
        .I2(Q[1]),
        .O(\dist_out[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dist_out_buffer[0]_i_1 
       (.I0(state_n[1]),
        .I1(\dist_out_buffer_reg_n_0_[0] ),
        .O(dist_out_buffer));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[10]_i_1 
       (.I0(state_n[1]),
        .I1(in3[10]),
        .O(\dist_out_buffer[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[11]_i_1 
       (.I0(state_n[1]),
        .I1(in3[11]),
        .O(\dist_out_buffer[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[12]_i_1 
       (.I0(state_n[1]),
        .I1(in3[12]),
        .O(\dist_out_buffer[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[13]_i_1 
       (.I0(state_n[1]),
        .I1(in3[13]),
        .O(\dist_out_buffer[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[14]_i_1 
       (.I0(state_n[1]),
        .I1(in3[14]),
        .O(\dist_out_buffer[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[15]_i_1 
       (.I0(state_n[1]),
        .I1(in3[15]),
        .O(\dist_out_buffer[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[16]_i_1 
       (.I0(state_n[1]),
        .I1(in3[16]),
        .O(\dist_out_buffer[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[17]_i_1 
       (.I0(state_n[1]),
        .I1(in3[17]),
        .O(\dist_out_buffer[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[18]_i_1 
       (.I0(state_n[1]),
        .I1(in3[18]),
        .O(\dist_out_buffer[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[19]_i_1 
       (.I0(state_n[1]),
        .I1(in3[19]),
        .O(\dist_out_buffer[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[1]_i_1 
       (.I0(state_n[1]),
        .I1(in3[1]),
        .O(\dist_out_buffer[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[20]_i_1 
       (.I0(state_n[1]),
        .I1(in3[20]),
        .O(\dist_out_buffer[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[21]_i_1 
       (.I0(state_n[1]),
        .I1(in3[21]),
        .O(\dist_out_buffer[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[22]_i_1 
       (.I0(state_n[1]),
        .I1(in3[22]),
        .O(\dist_out_buffer[22]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAA10)) 
    \dist_out_buffer[23]_i_1 
       (.I0(state_n[0]),
        .I1(\resync_start_reg_n_0_[2] ),
        .I2(p_1_in3_in),
        .I3(state_n[1]),
        .O(\dist_out_buffer[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[23]_i_2 
       (.I0(state_n[1]),
        .I1(in3[23]),
        .O(\dist_out_buffer[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[2]_i_1 
       (.I0(state_n[1]),
        .I1(in3[2]),
        .O(\dist_out_buffer[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[3]_i_1 
       (.I0(state_n[1]),
        .I1(in3[3]),
        .O(\dist_out_buffer[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[4]_i_1 
       (.I0(state_n[1]),
        .I1(in3[4]),
        .O(\dist_out_buffer[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[5]_i_1 
       (.I0(state_n[1]),
        .I1(in3[5]),
        .O(\dist_out_buffer[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[6]_i_1 
       (.I0(state_n[1]),
        .I1(in3[6]),
        .O(\dist_out_buffer[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[7]_i_1 
       (.I0(state_n[1]),
        .I1(in3[7]),
        .O(\dist_out_buffer[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[8]_i_1 
       (.I0(state_n[1]),
        .I1(in3[8]),
        .O(\dist_out_buffer[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dist_out_buffer[9]_i_1 
       (.I0(state_n[1]),
        .I1(in3[9]),
        .O(\dist_out_buffer[9]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[0] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(dist_out_buffer),
        .Q(\dist_out_buffer_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[10] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[10]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[10] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[11] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[11]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[11] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[12] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[12]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[12] ));
  CARRY4 \dist_out_buffer_reg[12]_i_2 
       (.CI(\dist_out_buffer_reg[8]_i_2_n_0 ),
        .CO({\dist_out_buffer_reg[12]_i_2_n_0 ,\dist_out_buffer_reg[12]_i_2_n_1 ,\dist_out_buffer_reg[12]_i_2_n_2 ,\dist_out_buffer_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in3[12:9]),
        .S({\dist_out_buffer_reg_n_0_[12] ,\dist_out_buffer_reg_n_0_[11] ,\dist_out_buffer_reg_n_0_[10] ,\dist_out_buffer_reg_n_0_[9] }));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[13] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[13]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[13] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[14] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[14]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[14] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[15] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[15]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[15] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[16] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[16]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[16] ));
  CARRY4 \dist_out_buffer_reg[16]_i_2 
       (.CI(\dist_out_buffer_reg[12]_i_2_n_0 ),
        .CO({\dist_out_buffer_reg[16]_i_2_n_0 ,\dist_out_buffer_reg[16]_i_2_n_1 ,\dist_out_buffer_reg[16]_i_2_n_2 ,\dist_out_buffer_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in3[16:13]),
        .S({\dist_out_buffer_reg_n_0_[16] ,\dist_out_buffer_reg_n_0_[15] ,\dist_out_buffer_reg_n_0_[14] ,\dist_out_buffer_reg_n_0_[13] }));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[17] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[17]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[17] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[18] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[18]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[18] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[19] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[19]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[19] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[1] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[1]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[20] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[20]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[20] ));
  CARRY4 \dist_out_buffer_reg[20]_i_2 
       (.CI(\dist_out_buffer_reg[16]_i_2_n_0 ),
        .CO({\dist_out_buffer_reg[20]_i_2_n_0 ,\dist_out_buffer_reg[20]_i_2_n_1 ,\dist_out_buffer_reg[20]_i_2_n_2 ,\dist_out_buffer_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in3[20:17]),
        .S({\dist_out_buffer_reg_n_0_[20] ,\dist_out_buffer_reg_n_0_[19] ,\dist_out_buffer_reg_n_0_[18] ,\dist_out_buffer_reg_n_0_[17] }));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[21] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[21]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[21] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[22] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[22]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[22] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[23] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[23]_i_2_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[23] ));
  CARRY4 \dist_out_buffer_reg[23]_i_3 
       (.CI(\dist_out_buffer_reg[20]_i_2_n_0 ),
        .CO({\NLW_dist_out_buffer_reg[23]_i_3_CO_UNCONNECTED [3:2],\dist_out_buffer_reg[23]_i_3_n_2 ,\dist_out_buffer_reg[23]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_dist_out_buffer_reg[23]_i_3_O_UNCONNECTED [3],in3[23:21]}),
        .S({1'b0,\dist_out_buffer_reg_n_0_[23] ,\dist_out_buffer_reg_n_0_[22] ,\dist_out_buffer_reg_n_0_[21] }));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[2] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[2]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[3] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[3]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[4] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[4]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[4] ));
  CARRY4 \dist_out_buffer_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\dist_out_buffer_reg[4]_i_2_n_0 ,\dist_out_buffer_reg[4]_i_2_n_1 ,\dist_out_buffer_reg[4]_i_2_n_2 ,\dist_out_buffer_reg[4]_i_2_n_3 }),
        .CYINIT(\dist_out_buffer_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in3[4:1]),
        .S({\dist_out_buffer_reg_n_0_[4] ,\dist_out_buffer_reg_n_0_[3] ,\dist_out_buffer_reg_n_0_[2] ,\dist_out_buffer_reg_n_0_[1] }));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[5] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[5]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[6] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[6]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[7] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[7]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[8] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[8]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[8] ));
  CARRY4 \dist_out_buffer_reg[8]_i_2 
       (.CI(\dist_out_buffer_reg[4]_i_2_n_0 ),
        .CO({\dist_out_buffer_reg[8]_i_2_n_0 ,\dist_out_buffer_reg[8]_i_2_n_1 ,\dist_out_buffer_reg[8]_i_2_n_2 ,\dist_out_buffer_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in3[8:5]),
        .S({\dist_out_buffer_reg_n_0_[8] ,\dist_out_buffer_reg_n_0_[7] ,\dist_out_buffer_reg_n_0_[6] ,\dist_out_buffer_reg_n_0_[5] }));
  FDCE #(
    .INIT(1'b0)) 
    \dist_out_buffer_reg[9] 
       (.C(clk),
        .CE(\dist_out_buffer[23]_i_1_n_0 ),
        .CLR(Q[1]),
        .D(\dist_out_buffer[9]_i_1_n_0 ),
        .Q(\dist_out_buffer_reg_n_0_[9] ));
  FDRE \dist_out_reg[0] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[0] ),
        .Q(dist_out[0]),
        .R(1'b0));
  FDRE \dist_out_reg[10] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[10] ),
        .Q(dist_out[10]),
        .R(1'b0));
  FDRE \dist_out_reg[11] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[11] ),
        .Q(dist_out[11]),
        .R(1'b0));
  FDRE \dist_out_reg[12] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[12] ),
        .Q(dist_out[12]),
        .R(1'b0));
  FDRE \dist_out_reg[13] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[13] ),
        .Q(dist_out[13]),
        .R(1'b0));
  FDRE \dist_out_reg[14] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[14] ),
        .Q(dist_out[14]),
        .R(1'b0));
  FDRE \dist_out_reg[15] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[15] ),
        .Q(dist_out[15]),
        .R(1'b0));
  FDRE \dist_out_reg[16] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[16] ),
        .Q(dist_out[16]),
        .R(1'b0));
  FDRE \dist_out_reg[17] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[17] ),
        .Q(dist_out[17]),
        .R(1'b0));
  FDRE \dist_out_reg[18] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[18] ),
        .Q(dist_out[18]),
        .R(1'b0));
  FDRE \dist_out_reg[19] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[19] ),
        .Q(dist_out[19]),
        .R(1'b0));
  FDRE \dist_out_reg[1] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[1] ),
        .Q(dist_out[1]),
        .R(1'b0));
  FDRE \dist_out_reg[20] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[20] ),
        .Q(dist_out[20]),
        .R(1'b0));
  FDRE \dist_out_reg[21] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[21] ),
        .Q(dist_out[21]),
        .R(1'b0));
  FDRE \dist_out_reg[22] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[22] ),
        .Q(dist_out[22]),
        .R(1'b0));
  FDRE \dist_out_reg[23] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[23] ),
        .Q(dist_out[23]),
        .R(1'b0));
  FDRE \dist_out_reg[2] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[2] ),
        .Q(dist_out[2]),
        .R(1'b0));
  FDRE \dist_out_reg[3] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[3] ),
        .Q(dist_out[3]),
        .R(1'b0));
  FDRE \dist_out_reg[4] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[4] ),
        .Q(dist_out[4]),
        .R(1'b0));
  FDRE \dist_out_reg[5] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[5] ),
        .Q(dist_out[5]),
        .R(1'b0));
  FDRE \dist_out_reg[6] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[6] ),
        .Q(dist_out[6]),
        .R(1'b0));
  FDRE \dist_out_reg[7] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[7] ),
        .Q(dist_out[7]),
        .R(1'b0));
  FDRE \dist_out_reg[8] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[8] ),
        .Q(dist_out[8]),
        .R(1'b0));
  FDRE \dist_out_reg[9] 
       (.C(clk),
        .CE(\dist_out[23]_i_1_n_0 ),
        .D(\dist_out_buffer_reg_n_0_[9] ),
        .Q(dist_out[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFF0FF40400000)) 
    oob_buffer_i_1
       (.I0(oob_buffer1__0),
        .I1(oob_buffer0__15),
        .I2(state_n[0]),
        .I3(ready_buffer1__0),
        .I4(state_n[1]),
        .I5(oob_buffer),
        .O(oob_buffer_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    oob_buffer_i_2
       (.I0(\resync_echo_reg_n_0_[2] ),
        .I1(p_1_in),
        .O(oob_buffer1__0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    oob_buffer_i_3
       (.I0(p_1_in3_in),
        .I1(\resync_start_reg_n_0_[2] ),
        .O(ready_buffer1__0));
  FDCE oob_buffer_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(oob_buffer_i_1_n_0),
        .Q(oob_buffer));
  FDCE oob_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(oob_buffer),
        .Q(oob));
  LUT5 #(
    .INIT(32'hFFFB000B)) 
    ready_buffer_i_1
       (.I0(\resync_start_reg_n_0_[2] ),
        .I1(p_1_in3_in),
        .I2(state_n[0]),
        .I3(state_n[1]),
        .I4(ready_buffer),
        .O(ready_buffer_i_1_n_0));
  FDCE ready_buffer_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(ready_buffer_i_1_n_0),
        .Q(ready_buffer));
  FDCE ready_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(ready_buffer),
        .Q(ready));
  FDRE \resync_echo_reg[1] 
       (.C(clk),
        .CE(\resync_start[1]_i_1_n_0 ),
        .D(echo),
        .Q(p_1_in),
        .R(1'b0));
  FDRE \resync_echo_reg[2] 
       (.C(clk),
        .CE(\resync_start[1]_i_1_n_0 ),
        .D(p_1_in),
        .Q(\resync_echo_reg_n_0_[2] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \resync_start[1]_i_1 
       (.I0(Q[1]),
        .O(\resync_start[1]_i_1_n_0 ));
  FDRE \resync_start_reg[1] 
       (.C(clk),
        .CE(\resync_start[1]_i_1_n_0 ),
        .D(Q[0]),
        .Q(p_1_in3_in),
        .R(1'b0));
  FDRE \resync_start_reg[2] 
       (.C(clk),
        .CE(\resync_start[1]_i_1_n_0 ),
        .D(p_1_in3_in),
        .Q(\resync_start_reg_n_0_[2] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFF0FFF00000404)) 
    trig_buffer_i_1
       (.I0(\resync_start_reg_n_0_[2] ),
        .I1(p_1_in3_in),
        .I2(state_n[0]),
        .I3(trig_buffer_i_2_n_0),
        .I4(state_n[1]),
        .I5(trig_buffer),
        .O(trig_buffer_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    trig_buffer_i_2
       (.I0(trig_buffer_i_3_n_0),
        .I1(trig_buffer_i_4_n_0),
        .I2(trig_buffer_i_5_n_0),
        .I3(trig_buffer_i_6_n_0),
        .I4(trig_buffer_i_7_n_0),
        .I5(trig_buffer_i_8_n_0),
        .O(trig_buffer_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    trig_buffer_i_3
       (.I0(state_n1[24]),
        .I1(state_n1[25]),
        .I2(state_n1[26]),
        .I3(state_n1[27]),
        .I4(state_n1[28]),
        .I5(state_n1[29]),
        .O(trig_buffer_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF7F)) 
    trig_buffer_i_4
       (.I0(state_n1[7]),
        .I1(state_n1[8]),
        .I2(state_n1[6]),
        .I3(state_n1[9]),
        .I4(state_n1[10]),
        .I5(state_n1[11]),
        .O(trig_buffer_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    trig_buffer_i_5
       (.I0(state_n1[18]),
        .I1(state_n1[19]),
        .I2(state_n1[20]),
        .I3(state_n1[21]),
        .I4(state_n1[22]),
        .I5(state_n1[23]),
        .O(trig_buffer_i_5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    trig_buffer_i_6
       (.I0(state_n1[12]),
        .I1(state_n1[13]),
        .I2(state_n1[14]),
        .I3(state_n1[15]),
        .I4(state_n1[16]),
        .I5(state_n1[17]),
        .O(trig_buffer_i_6_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    trig_buffer_i_7
       (.I0(state_n1[30]),
        .I1(state_n1[31]),
        .O(trig_buffer_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    trig_buffer_i_8
       (.I0(state_n1[2]),
        .I1(state_n1[1]),
        .I2(trig_count_reg[0]),
        .I3(state_n1[3]),
        .I4(state_n1[4]),
        .I5(state_n1[5]),
        .O(trig_buffer_i_8_n_0));
  FDCE trig_buffer_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(trig_buffer_i_1_n_0),
        .Q(trig_buffer));
  CARRY4 trig_buffer_reg_i_10
       (.CI(trig_buffer_reg_i_9_n_0),
        .CO({trig_buffer_reg_i_10_n_0,trig_buffer_reg_i_10_n_1,trig_buffer_reg_i_10_n_2,trig_buffer_reg_i_10_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[28:25]),
        .S(trig_count_reg[28:25]));
  CARRY4 trig_buffer_reg_i_11
       (.CI(trig_buffer_reg_i_10_n_0),
        .CO({NLW_trig_buffer_reg_i_11_CO_UNCONNECTED[3:2],trig_buffer_reg_i_11_n_2,trig_buffer_reg_i_11_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_trig_buffer_reg_i_11_O_UNCONNECTED[3],state_n1[31:29]}),
        .S({1'b0,trig_count_reg[31:29]}));
  CARRY4 trig_buffer_reg_i_12
       (.CI(trig_buffer_reg_i_16_n_0),
        .CO({trig_buffer_reg_i_12_n_0,trig_buffer_reg_i_12_n_1,trig_buffer_reg_i_12_n_2,trig_buffer_reg_i_12_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[8:5]),
        .S(trig_count_reg[8:5]));
  CARRY4 trig_buffer_reg_i_13
       (.CI(trig_buffer_reg_i_12_n_0),
        .CO({trig_buffer_reg_i_13_n_0,trig_buffer_reg_i_13_n_1,trig_buffer_reg_i_13_n_2,trig_buffer_reg_i_13_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[12:9]),
        .S(trig_count_reg[12:9]));
  CARRY4 trig_buffer_reg_i_14
       (.CI(trig_buffer_reg_i_15_n_0),
        .CO({trig_buffer_reg_i_14_n_0,trig_buffer_reg_i_14_n_1,trig_buffer_reg_i_14_n_2,trig_buffer_reg_i_14_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[20:17]),
        .S(trig_count_reg[20:17]));
  CARRY4 trig_buffer_reg_i_15
       (.CI(trig_buffer_reg_i_13_n_0),
        .CO({trig_buffer_reg_i_15_n_0,trig_buffer_reg_i_15_n_1,trig_buffer_reg_i_15_n_2,trig_buffer_reg_i_15_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[16:13]),
        .S(trig_count_reg[16:13]));
  CARRY4 trig_buffer_reg_i_16
       (.CI(1'b0),
        .CO({trig_buffer_reg_i_16_n_0,trig_buffer_reg_i_16_n_1,trig_buffer_reg_i_16_n_2,trig_buffer_reg_i_16_n_3}),
        .CYINIT(trig_count_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[4:1]),
        .S(trig_count_reg[4:1]));
  CARRY4 trig_buffer_reg_i_9
       (.CI(trig_buffer_reg_i_14_n_0),
        .CO({trig_buffer_reg_i_9_n_0,trig_buffer_reg_i_9_n_1,trig_buffer_reg_i_9_n_2,trig_buffer_reg_i_9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(state_n1[24:21]),
        .S(trig_count_reg[24:21]));
  LUT3 #(
    .INIT(8'h04)) 
    \trig_count[0]_i_1 
       (.I0(state_n[1]),
        .I1(state_n[0]),
        .I2(Q[1]),
        .O(\trig_count[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[0]_i_3 
       (.I0(trig_count_reg[0]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[0]_i_4 
       (.I0(trig_count_reg[3]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[0]_i_5 
       (.I0(trig_count_reg[2]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[0]_i_6 
       (.I0(trig_count_reg[1]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \trig_count[0]_i_7 
       (.I0(trig_count_reg[0]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[12]_i_2 
       (.I0(trig_count_reg[15]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[12]_i_3 
       (.I0(trig_count_reg[14]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[12]_i_4 
       (.I0(trig_count_reg[13]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[12]_i_5 
       (.I0(trig_count_reg[12]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[16]_i_2 
       (.I0(trig_count_reg[19]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[16]_i_3 
       (.I0(trig_count_reg[18]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[16]_i_4 
       (.I0(trig_count_reg[17]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[16]_i_5 
       (.I0(trig_count_reg[16]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[20]_i_2 
       (.I0(trig_count_reg[23]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[20]_i_3 
       (.I0(trig_count_reg[22]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[20]_i_4 
       (.I0(trig_count_reg[21]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[20]_i_5 
       (.I0(trig_count_reg[20]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[24]_i_2 
       (.I0(trig_count_reg[27]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[24]_i_3 
       (.I0(trig_count_reg[26]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[24]_i_4 
       (.I0(trig_count_reg[25]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[24]_i_5 
       (.I0(trig_count_reg[24]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[28]_i_2 
       (.I0(trig_count_reg[31]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[28]_i_3 
       (.I0(trig_count_reg[30]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[28]_i_4 
       (.I0(trig_count_reg[29]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[28]_i_5 
       (.I0(trig_count_reg[28]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[28]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[4]_i_2 
       (.I0(trig_count_reg[7]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[4]_i_3 
       (.I0(trig_count_reg[6]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[4]_i_4 
       (.I0(trig_count_reg[5]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[4]_i_5 
       (.I0(trig_count_reg[4]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[8]_i_2 
       (.I0(trig_count_reg[11]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[8]_i_3 
       (.I0(trig_count_reg[10]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[8]_i_4 
       (.I0(trig_count_reg[9]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_count[8]_i_5 
       (.I0(trig_count_reg[8]),
        .I1(trig_buffer_i_2_n_0),
        .O(\trig_count[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[0] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[0]_i_2_n_7 ),
        .Q(trig_count_reg[0]),
        .R(1'b0));
  CARRY4 \trig_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\trig_count_reg[0]_i_2_n_0 ,\trig_count_reg[0]_i_2_n_1 ,\trig_count_reg[0]_i_2_n_2 ,\trig_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\trig_count[0]_i_3_n_0 }),
        .O({\trig_count_reg[0]_i_2_n_4 ,\trig_count_reg[0]_i_2_n_5 ,\trig_count_reg[0]_i_2_n_6 ,\trig_count_reg[0]_i_2_n_7 }),
        .S({\trig_count[0]_i_4_n_0 ,\trig_count[0]_i_5_n_0 ,\trig_count[0]_i_6_n_0 ,\trig_count[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[10] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[8]_i_1_n_5 ),
        .Q(trig_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[11] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[8]_i_1_n_4 ),
        .Q(trig_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[12] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[12]_i_1_n_7 ),
        .Q(trig_count_reg[12]),
        .R(1'b0));
  CARRY4 \trig_count_reg[12]_i_1 
       (.CI(\trig_count_reg[8]_i_1_n_0 ),
        .CO({\trig_count_reg[12]_i_1_n_0 ,\trig_count_reg[12]_i_1_n_1 ,\trig_count_reg[12]_i_1_n_2 ,\trig_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[12]_i_1_n_4 ,\trig_count_reg[12]_i_1_n_5 ,\trig_count_reg[12]_i_1_n_6 ,\trig_count_reg[12]_i_1_n_7 }),
        .S({\trig_count[12]_i_2_n_0 ,\trig_count[12]_i_3_n_0 ,\trig_count[12]_i_4_n_0 ,\trig_count[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[13] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[12]_i_1_n_6 ),
        .Q(trig_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[14] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[12]_i_1_n_5 ),
        .Q(trig_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[15] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[12]_i_1_n_4 ),
        .Q(trig_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[16] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[16]_i_1_n_7 ),
        .Q(trig_count_reg[16]),
        .R(1'b0));
  CARRY4 \trig_count_reg[16]_i_1 
       (.CI(\trig_count_reg[12]_i_1_n_0 ),
        .CO({\trig_count_reg[16]_i_1_n_0 ,\trig_count_reg[16]_i_1_n_1 ,\trig_count_reg[16]_i_1_n_2 ,\trig_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[16]_i_1_n_4 ,\trig_count_reg[16]_i_1_n_5 ,\trig_count_reg[16]_i_1_n_6 ,\trig_count_reg[16]_i_1_n_7 }),
        .S({\trig_count[16]_i_2_n_0 ,\trig_count[16]_i_3_n_0 ,\trig_count[16]_i_4_n_0 ,\trig_count[16]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[17] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[16]_i_1_n_6 ),
        .Q(trig_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[18] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[16]_i_1_n_5 ),
        .Q(trig_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[19] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[16]_i_1_n_4 ),
        .Q(trig_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[1] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[0]_i_2_n_6 ),
        .Q(trig_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[20] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[20]_i_1_n_7 ),
        .Q(trig_count_reg[20]),
        .R(1'b0));
  CARRY4 \trig_count_reg[20]_i_1 
       (.CI(\trig_count_reg[16]_i_1_n_0 ),
        .CO({\trig_count_reg[20]_i_1_n_0 ,\trig_count_reg[20]_i_1_n_1 ,\trig_count_reg[20]_i_1_n_2 ,\trig_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[20]_i_1_n_4 ,\trig_count_reg[20]_i_1_n_5 ,\trig_count_reg[20]_i_1_n_6 ,\trig_count_reg[20]_i_1_n_7 }),
        .S({\trig_count[20]_i_2_n_0 ,\trig_count[20]_i_3_n_0 ,\trig_count[20]_i_4_n_0 ,\trig_count[20]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[21] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[20]_i_1_n_6 ),
        .Q(trig_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[22] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[20]_i_1_n_5 ),
        .Q(trig_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[23] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[20]_i_1_n_4 ),
        .Q(trig_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[24] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[24]_i_1_n_7 ),
        .Q(trig_count_reg[24]),
        .R(1'b0));
  CARRY4 \trig_count_reg[24]_i_1 
       (.CI(\trig_count_reg[20]_i_1_n_0 ),
        .CO({\trig_count_reg[24]_i_1_n_0 ,\trig_count_reg[24]_i_1_n_1 ,\trig_count_reg[24]_i_1_n_2 ,\trig_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[24]_i_1_n_4 ,\trig_count_reg[24]_i_1_n_5 ,\trig_count_reg[24]_i_1_n_6 ,\trig_count_reg[24]_i_1_n_7 }),
        .S({\trig_count[24]_i_2_n_0 ,\trig_count[24]_i_3_n_0 ,\trig_count[24]_i_4_n_0 ,\trig_count[24]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[25] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[24]_i_1_n_6 ),
        .Q(trig_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[26] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[24]_i_1_n_5 ),
        .Q(trig_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[27] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[24]_i_1_n_4 ),
        .Q(trig_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[28] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[28]_i_1_n_7 ),
        .Q(trig_count_reg[28]),
        .R(1'b0));
  CARRY4 \trig_count_reg[28]_i_1 
       (.CI(\trig_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_trig_count_reg[28]_i_1_CO_UNCONNECTED [3],\trig_count_reg[28]_i_1_n_1 ,\trig_count_reg[28]_i_1_n_2 ,\trig_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[28]_i_1_n_4 ,\trig_count_reg[28]_i_1_n_5 ,\trig_count_reg[28]_i_1_n_6 ,\trig_count_reg[28]_i_1_n_7 }),
        .S({\trig_count[28]_i_2_n_0 ,\trig_count[28]_i_3_n_0 ,\trig_count[28]_i_4_n_0 ,\trig_count[28]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[29] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[28]_i_1_n_6 ),
        .Q(trig_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[2] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[0]_i_2_n_5 ),
        .Q(trig_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[30] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[28]_i_1_n_5 ),
        .Q(trig_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[31] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[28]_i_1_n_4 ),
        .Q(trig_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[3] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[0]_i_2_n_4 ),
        .Q(trig_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[4] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[4]_i_1_n_7 ),
        .Q(trig_count_reg[4]),
        .R(1'b0));
  CARRY4 \trig_count_reg[4]_i_1 
       (.CI(\trig_count_reg[0]_i_2_n_0 ),
        .CO({\trig_count_reg[4]_i_1_n_0 ,\trig_count_reg[4]_i_1_n_1 ,\trig_count_reg[4]_i_1_n_2 ,\trig_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[4]_i_1_n_4 ,\trig_count_reg[4]_i_1_n_5 ,\trig_count_reg[4]_i_1_n_6 ,\trig_count_reg[4]_i_1_n_7 }),
        .S({\trig_count[4]_i_2_n_0 ,\trig_count[4]_i_3_n_0 ,\trig_count[4]_i_4_n_0 ,\trig_count[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[5] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[4]_i_1_n_6 ),
        .Q(trig_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[6] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[4]_i_1_n_5 ),
        .Q(trig_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[7] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[4]_i_1_n_4 ),
        .Q(trig_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[8] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[8]_i_1_n_7 ),
        .Q(trig_count_reg[8]),
        .R(1'b0));
  CARRY4 \trig_count_reg[8]_i_1 
       (.CI(\trig_count_reg[4]_i_1_n_0 ),
        .CO({\trig_count_reg[8]_i_1_n_0 ,\trig_count_reg[8]_i_1_n_1 ,\trig_count_reg[8]_i_1_n_2 ,\trig_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_count_reg[8]_i_1_n_4 ,\trig_count_reg[8]_i_1_n_5 ,\trig_count_reg[8]_i_1_n_6 ,\trig_count_reg[8]_i_1_n_7 }),
        .S({\trig_count[8]_i_2_n_0 ,\trig_count[8]_i_3_n_0 ,\trig_count[8]_i_4_n_0 ,\trig_count[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_count_reg[9] 
       (.C(clk),
        .CE(\trig_count[0]_i_1_n_0 ),
        .D(\trig_count_reg[8]_i_1_n_6 ),
        .Q(trig_count_reg[9]),
        .R(1'b0));
  FDCE trig_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(trig_buffer),
        .Q(trig));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

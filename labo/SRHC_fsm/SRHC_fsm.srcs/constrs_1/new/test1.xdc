set_property PACKAGE_PIN F14 [get_ports trig_0]
set_property PACKAGE_PIN F13 [get_ports echo_0]
set_property IOSTANDARD LVCMOS33 [get_ports echo_0]
set_property IOSTANDARD LVCMOS33 [get_ports trig_0]

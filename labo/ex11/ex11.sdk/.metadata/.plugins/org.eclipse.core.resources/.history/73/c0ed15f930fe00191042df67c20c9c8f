/*
    Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
    Copyright (C) 2012 - 2018 Xilinx, Inc. All Rights Reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software. If you wish to use our Amazon
    FreeRTOS name, please do so in a fair use way that does not cause confusion.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    http://www.FreeRTOS.org
    http://aws.amazon.com/freertos


    1 tab == 4 spaces!
*/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/* Xilinx includes. */
#include "xil_printf.h"
#include "xparameters.h"
#include "xgpiops.h"
#include "xstatus.h"

#define TIMER_ID_3	3
#define TIMER_ID_5	5
#define DELAY_3_SECONDS	3000UL
#define DELAY_5_SECONDS	5000UL
#define TIMER_CHECK_THRESHOLD	9

#define LED_1 51
#define LED_2 52
/*-----------------------------------------------------------*/

/* The Tx and Rx tasks as described at the top of this file. */
//static void timer3callback(TimerHandle_t pxTimer);
//static void timer5callback(TimerHandle_t pxTimer);
static void vTimerCallback( TimerHandle_t pxTimer );
/*-----------------------------------------------------------*/

XGpioPs Gpio;
static TimerHandle_t xTimer3s= NULL;
static TimerHandle_t xTimer5s= NULL;
char HWstring[15] = "Hello World";
long RxtaskCntr = 0;
u8 led_1_data = 0x00;
u8 led_2_data = 0x00;

int main( void )
{
	const TickType_t x3seconds = pdMS_TO_TICKS( DELAY_3_SECONDS );
	const TickType_t x5seconds = pdMS_TO_TICKS( DELAY_5_SECONDS );

	XGpioPs_Config *GPIOConfigPtr;

	GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, GPIOConfigPtr,GPIOConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, LED_1, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, LED_1, 1);
	XGpioPs_SetDirectionPin(&Gpio, LED_2, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, LED_2, 1);
	XGpioPs_WritePin(&Gpio, LED_1,0x0);
	XGpioPs_WritePin(&Gpio, LED_2,0x0);

	xil_printf( "Running ex11\r\n" );

	/* Create a timer with a timer expiry of 10 seconds. The timer would expire
	 after 10 seconds and the timer call back would get called. In the timer call back
	 checks are done to ensure that the tasks have been running properly till then.
	 The tasks are deleted in the timer call back and a message is printed to convey that
	 the example has run successfully.
	 The timer expiry is set to 10 seconds and the timer set to not auto reload. */
	xTimer3s = xTimerCreate( (const char *) "Timer3s",
							x3seconds,
							pdTRUE,
							(void *) TIMER_ID_3,
							vTimerCallback);

	xTimer5s = xTimerCreate( (const char *) "Timer5s",
								x5seconds,
								pdFALSE,
								(void *) TIMER_ID_5,
								vTimerCallback);
	/* Check the timer was created. */
	configASSERT( xTimer3s );
	configASSERT( xTimer5s );

	/* start the timer with a block time of 0 ticks. This means as soon
	   as the schedule starts the timer will start running and will expire after
	   10 seconds */
	xTimerStart( xTimer3s, 0 );
	xTimerStart( xTimer5s, 0 );

	/* Start the tasks and timer running. */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	for( ;; );
}

/*-----------------------------------------------------------*/
static void vTimerCallback( TimerHandle_t pxTimer )
{
	long lTimerId;
	configASSERT( pxTimer );

	lTimerId = ( long ) pvTimerGetTimerID( pxTimer );

	if(lTimerId == TIMER_ID_3) {
		led_1_data ^= 0x01;
		XGpioPs_WritePin(&Gpio, LED_1,led_1_data);
		xil_printf("Timer id 3, led_1 data: %d\r\n", led_1_data);
	} else if(lTimerId == TIMER_ID_5){
		led_2_data ^= 0x01;
		XGpioPs_WritePin(&Gpio, LED_2,led_2_data);
		xil_printf("Timer id 5\r\n");
	} else {
		xil_printf("Unknown timer.\r\n");
	}
}


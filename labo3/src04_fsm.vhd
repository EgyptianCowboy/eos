library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity src04 is

  port (
    clk       : in std_logic;                -- Clk
    reset     : in std_logic;                -- Reset
    trig      : in std_logic;                -- Trigger for the src04
    echo      : in std_logic;                -- Echo of the wave
    ready     : out std_logic;
    triglatch : out std_logic;
    dist      : out  std_logic_vector(25 downto 0));  -- Distance of the wave

end entity src04;

architecture Behavorial of src04 is
  TYPE state_type IS (trigwait, outtrig, echowait);  -- Define the states
  signal state_reg, state_next : state_type;

  signal distcnt : std_logic_vector(25 downto 0) := (others => '0');
  signal trigcnt: std_logic_vector(9 downto 0) := "0000000000";
  signal readybuf: std_logic := '0';

begin  -- architecture src04_fsm
  process(clk, reset)
    begin
        if reset = '1' then
            state_reg <= trigwait;
        elsif clk'event and clk = '1' then
          state_reg <= state_next;
          if state_reg = outtrig then
            trigcnt <= std_logic_vector(unsigned(trigcnt)+1);
          elsif state_next = echowait then
            distcnt <= std_logic_vector(unsigned(distcnt)+1);
          end if;
        end if;
    end process;

    process(echo, trig, distcnt, trigcnt, state_reg)
    begin
        state_next <= state_reg; -- default state_next
        -- default outputs
        ready <= '0';
        triglatch <= '0';
        case state_reg is
          when trigwait =>
            if trig = '1' then -- if (input1 = '01') then
              triglatch <= '1';
              readybuf <= '0';
              state_next <= outtrig;
              distcnt <= (others => '0');
            end if;
            when outtrig =>
            if trigcnt = "1000011100" then
              state_next <= echowait;
              triglatch <= '0';
              trigcnt <= (others => '0');
            end if;
          when echowait =>
            if echo = '1' then
              state_next <= trigwait;
              readybuf <= '1';
            end if;
        end case;
    end process;

    dist <= distcnt;
    ready <= readybuf;

end architecture Behavorial;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.10.2019 12:44:37
-- Design Name: 
-- Module Name: sr04 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sr04 is
  Port (clk:  in  std_logic;
        trig: in  std_logic;
        echo: in  std_logic;
        ready: out std_logic;
        distance: out std_logic_vector(25 downto 0);
        triglatch: out std_logic);
end sr04;

architecture Behavioral of sr04 is
  signal start: std_logic;
  signal trigstart: std_logic;
  signal distcnt : std_logic_vector(25 downto 0) := (others => '0');
  signal trigcnt: std_logic_vector(9 downto 0) := "0000000000";
  signal trigwait: std_logic_vector(9 downto 0) := "1000011100"; -- Clock cycles
                                                                 -- to keep trig
                                                                 -- high, 540
  signal echowait: std_logic := '0';
begin

  -- purpose: Get distance clock cycles
  -- type   : sequential
  -- inputs : clk, rst, triglatch
  -- outputs: dist
  getdist: process (clk) is
  begin  -- process getdist
    if clk'event and clk = '1' then  -- rising clock edge
      if echo = '0' and echowait = '0' then
        start <= '0';
        if trig = '1' then
          ready <= '0';
          distcnt <= (others => '0');
          trigstart <= '1';
        end if;
      else
        trigstart <= '0';
        trigcnt <= (others => '0');
        distcnt <= std_logic_vector(unsigned(distcnt) + 1);
        start <= '1';
        ready <= '1';
        echowait <= '0';
      end if;

      if trigstart = '1' then
        echowait <= '1';
        if trigcnt = trigwait then
          trigstart <= '0';
          triglatch <= '0';
        else
          trigcnt <= std_logic_vector(unsigned(trigcnt) + 1);
          triglatch <= '1';
        end if;
      end if;

    end if;
  end process getdist;

  with start select distance <=
    distcnt when '0',
    (others => '0') when others;
end Behavioral;

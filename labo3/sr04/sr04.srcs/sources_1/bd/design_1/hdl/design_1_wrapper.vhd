--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
--Date        : Tue Oct 15 14:31:15 2019
--Host        : t480s running 64-bit unknown
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    echo_0 : in STD_LOGIC;
    reset_rtl_0 : in STD_LOGIC;
    triglatch_0 : out STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    echo_0 : in STD_LOGIC;
    triglatch_0 : out STD_LOGIC;
    reset_rtl_0 : in STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      echo_0 => echo_0,
      reset_rtl_0 => reset_rtl_0,
      triglatch_0 => triglatch_0
    );
end STRUCTURE;

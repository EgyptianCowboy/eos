library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity src04 is

  port (
    clk       : in std_logic;                -- Clk
    reset     : in std_logic;                -- Reset
    trig      : in std_logic;                -- Trigger for the src04
    echo      : in std_logic;                -- Echo of the wave
    state_enc : out std_logic_vector(3 downto 0);
    ready     : out std_logic;
    triglatch : out std_logic;
    distance  : out  std_logic_vector(25 downto 0));  -- Distance of the wave

end entity src04;

architecture Behavorial of src04 is
  TYPE state_type IS (trigwait, outtrig, echozero, echowait);  -- Define the states
  signal state_reg : state_type := trigwait;
  signal state_next : state_type := trigwait;

  signal distcnt : std_logic_vector(25 downto 0) := (others => '0');
  signal trigcnt: std_logic_vector(9 downto 0) := "0000000000";
  signal readybuf: std_logic := '0';
  signal statebuf: std_logic_vector(3 downto 0) := "0000";

begin  -- architecture src04_fsm
  process(clk, reset)
    begin
        if reset = '1' then
          state_reg <= trigwait;
        elsif clk'event and clk = '1' then
          state_reg <= state_next;
          if state_reg = outtrig then
            trigcnt <= std_logic_vector(unsigned(trigcnt)+1);
            distcnt <= (others => '0');
          elsif state_reg = echowait then
            distcnt <= std_logic_vector(unsigned(distcnt)+1);
            trigcnt <= (others => '0');
          end if;
        end if;
    end process;

    process(echo, trig, distcnt, trigcnt, state_reg)
    begin
      state_next <= state_reg; -- default state_next
      case state_reg is
        when trigwait =>
          distance <= distcnt;
          statebuf <= "0001";
          if trig'event and trig = '1' then
            triglatch <= '1';
            readybuf <= '0';
            state_next <= outtrig;
          end if;
        when outtrig =>
          statebuf <= "0010";
          if trigcnt = "1100000000" then
            state_next <= echozero;
            triglatch <= '0';
          end if;
        when echozero =>
          statebuf <= "0100";
          if echo = '1' then
            state_next <= echowait;
          end if;
        when echowait =>
          statebuf <= "1000";
          if echo = '0' then
            state_next <= trigwait;
            readybuf <= '1';
          end if;
      end case;
    end process;
    ready <= readybuf;
    state_enc <= statebuf;
end architecture Behavorial;

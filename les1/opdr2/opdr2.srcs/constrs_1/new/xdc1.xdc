set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sSeg[0]}]
set_property PACKAGE_PIN N13 [get_ports {sSeg[7]}]
set_property PACKAGE_PIN N14 [get_ports {sSeg[6]}]
set_property PACKAGE_PIN K13 [get_ports {sSeg[5]}]
set_property PACKAGE_PIN L13 [get_ports {sSeg[4]}]
set_property PACKAGE_PIN L14 [get_ports {sSeg[3]}]
set_property PACKAGE_PIN M14 [get_ports {sSeg[2]}]
set_property PACKAGE_PIN L15 [get_ports {sSeg[1]}]
set_property PACKAGE_PIN M15 [get_ports {sSeg[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports swin]
set_property PACKAGE_PIN E11 [get_ports swin]

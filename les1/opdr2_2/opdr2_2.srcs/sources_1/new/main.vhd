----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 24.09.2019 13:16:22
-- Design Name:
-- Module Name: looplicht - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity looplicht is
  Port (swin : in std_logic;
        sSeg : out std_logic_vector(7 downto 0));
end looplicht;

architecture Behavioral of looplicht is
  signal cnt : std_logic_vector(25 downto 0) := (others=>'0');
  signal i_clk : std_logic;
  signal temp : std_logic_vector(5 downto 0) := "000001";

  component vhdlnoclk is
    port (clk65MHz : out std_logic);
  end component;
begin

  clock_instance : vhdlnoclk
    port map (clk65MHz => i_clk);

  process(i_clk)
  begin
    if rising_edge(i_clk) then
      if cnt = "111111111111111111111111" then
        cnt <=(others=>'0');
        if(swin = '1') then
          temp <= std_logic_vector(rotate_right(unsigned(temp),1));
        else
          temp <= std_logic_vector(rotate_left(unsigned(temp),1));
        end if;
      end if;
      cnt <= std_logic_vector(unsigned(cnt) + 1);
    end if;
  end process;

   sSeg(5 downto 0) <= temp;
end Behavioral;

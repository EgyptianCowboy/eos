set_property IOSTANDARD LVCMOS33 [get_ports SWITCH1]

set_property IOSTANDARD LVCMOS33 [get_ports LEDG]
set_property IOSTANDARD LVCMOS33 [get_ports LEDR]
set_property PACKAGE_PIN E13 [get_ports LEDG]
set_property PACKAGE_PIN E12 [get_ports LEDR]
set_property PACKAGE_PIN E11 [get_ports SWITCH1]
